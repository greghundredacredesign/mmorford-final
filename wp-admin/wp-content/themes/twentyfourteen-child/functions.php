<?php

// Query for home page
function my_post_queries( $query ) {
	// do not alter the query on wp-admin pages and only alter it if it's the main query
	if (!is_admin() && $query->is_main_query()){

	    // alter the query for the home page
	    if( is_home() ){
			$query->set( 'posts_per_page', 10 );
			$query->set( 'post_type', array( 'writings', 'galleries', 'yoga_workshops', 'social_hashtag', 'post' ));
			$query->set( 'orderby', 'menu_order date' );
			$query->set( 'meta_query', array( 'relation' => 'OR', array( 'key' => 'headline_slider', 'value' => '1', 'compare' => '!=' ), array( 'key' => 'headline_slider', 'value' => '1', 'compare' => 'NOT EXISTS' )));
	    }
	}
}
add_action( 'pre_get_posts', 'my_post_queries' );

// Remove theme support ==============================/
function twentyfourteen_child_setup() {

	remove_theme_support( 'custom-header' );
	remove_theme_support( 'custom-background' );
}
add_action( 'after_setup_theme', 'twentyfourteen_child_setup', 11 );

// Remove twentyfourteen sidebars ==============================/
function remove_some_widgets() {
	unregister_sidebar( 'sidebar-1' );
	unregister_sidebar( 'sidebar-2' );
	unregister_sidebar( 'sidebar-3' );
}
add_action( 'widgets_init', 'remove_some_widgets', 11 );

// Thumbnails ==============================/
if ( function_exists( 'add_image_size' ) ) { 
	// Related Thumbnails
	add_image_size( 'module-thumb', 287, 287, true );
	add_image_size( 'hero-thumb', 842, 424, true );
}

// Excerpt ==============================/
function custom_excerpt_length( $length ) {
	return 14;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '&nbsp;&nbsp;<a href="' . get_permalink() . '" class="excerpt-link">read more &raquo;</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Enqueue Scripts and Style ==============================/
function child_scripts() {

	// Remove Parents
	wp_dequeue_style('genericons');
	wp_dequeue_style('twentyfourteen-lato');
	wp_dequeue_style('twentyfourteen-style');
	wp_dequeue_style('twentyfourteen-ie');

	wp_register_script(
    	'isotope',
        get_stylesheet_directory_uri() . '/js/isotope.js',
	    array( 'jquery' ),
    	'0.0.1',
    	false
	);

	wp_register_script(
		'waypoints',
		get_stylesheet_directory_uri() . '/js/waypoints.min.js',
		array( 'jquery' ),
		'2.0.5',
		true
	);

	wp_register_script(
		'waypoints-sticky',
		get_stylesheet_directory_uri() . '/js/waypoints-sticky.min.js',
		array( 'jquery' ),
		'2.0.5',
		true
	);

	wp_register_script(
		'infinite',
		get_stylesheet_directory_uri() . '/js/infinite-scroll.min.js',
		array( 'jquery' ),
		'2.0.5',
		true
	);

	wp_register_script(
		'magnific',
		get_stylesheet_directory_uri() . '/js/magnific-popup.min.js',
		array( 'jquery' ),
		'0.0.9',
		true
	);

	wp_register_script(
    	'main',
        get_stylesheet_directory_uri() . '/js/min/main.min.js',
	    array( 'jquery' ),
    	'0.0.1',
    	true
	);
	
	wp_enqueue_script( 'isotope' );
	wp_enqueue_script( 'waypoints' );
	wp_enqueue_script( 'waypoints-sticky' );
	wp_enqueue_script( 'infinite' );
	wp_enqueue_script( 'magnific' );
	wp_enqueue_script( 'main' );

	if( is_home() || is_page( '16' ) ) {

		wp_register_script(
			'slick',
			get_stylesheet_directory_uri() . '/js/slick.min.js',
			array( 'jquery' ),
			'0.0.1',
			true
		);

		wp_enqueue_script( 'slick' );
	}

	if ( is_home() ) {

		wp_register_script(
			'home-scripts',
			get_stylesheet_directory_uri() . '/js/min/home-scripts.min.js',
			array( 'slick' ),
			'0.0.1',
			true
		);

		wp_enqueue_script( 'home-scripts' );
	}

	if ( is_page( '16' ) ) {

		wp_register_script(
			'about-scripts',
			get_stylesheet_directory_uri() . '/js/min/about-scripts.min.js',
			array( 'slick' ),
			'0.0.1',
			true
		);

		wp_enqueue_script( 'about-scripts' );
	}

	if ( is_page( '35' ) ) {

		wp_register_script(
			'yoga-scripts',
			get_stylesheet_directory_uri() . '/js/yoga-scripts.js',
			array( 'jquery' ),
			'0.0.1',
			true
		);

		wp_enqueue_script( 'yoga-scripts' );
	}

	if ( ! is_home() ) {

		wp_register_script(
			'not-home',
			get_stylesheet_directory_uri() . '/js/min/scripts-not_home.min.js',
			array( 'jquery' ),
			'0.0.1',
			true
		);

		wp_enqueue_script( 'not-home' );
	}
}
add_action( 'wp_enqueue_scripts', 'child_scripts' );

// Custom Post Types ==============================/
add_action( 'init', 'custom_posttype' );

function custom_posttype() {

	register_post_type( 'writings', array(
		'labels' => array(
			'name' => __( 'Writings' ),
			'singular_name' => __( 'Writing' )
		),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'custom-fields',
			'revisions',
		),
	));

	register_post_type( 'galleries', array(
		'labels' => array(
			'name' => __( 'Galleries' ),
			'singular_name' => __( 'Gallery' )
		),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'custom-fields',
			'revisions',
		),
	));

	register_post_type( 'yoga_workshops', array(
		'labels' => array(
			'name' => __( 'Yoga' ),
			'singular_name' => __( 'Yoga' )
		),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'custom-fields',
			'revisions',
		),
	));

	register_post_type( 'about', array(
		'labels' => array(
			'name' => __( 'About' ),
			'singular_name' => __( 'About Post' )
		),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'custom-fields'
		),
	));

	register_post_type( 'logos', array(
		'labels' => array(
			'name' => __( 'Logos' ),
			'singular_name' => __( 'Logo' )
		),
		'public' => true,
		'has_archive' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'thumbnail',
		),
	));

};

// Custom Taxonomy For Writing Publisher ==============================/
function publisher_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Publishers', 'taxonomy general name' ),
		'singular_name'              => _x( 'Publisher', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Publishers' ),
		'popular_items'              => __( 'Common Publishers' ),
		'all_items'                  => __( 'All Publishers' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Publisher' ),
		'update_item'                => __( 'Update Publisher' ),
		'add_new_item'               => __( 'Add New Publisher' ),
		'new_item_name'              => __( 'New Publisher Name' ),
		'separate_items_with_commas' => __( 'Separate Publishers with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Publisher' ),
		'choose_from_most_used'      => __( 'Choose from the most common Publishers' ),
		'not_found'                  => __( 'No Publisher found.' ),
		'menu_name'                  => __( 'Publishers' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'publisher' ),
	);

	register_taxonomy( 'publishers', 'writings', $args );
}
add_action( 'init', 'publisher_taxonomy' );

// Custom Taxonomy For Yoga and Workshops ==============================/
function yoga_workshop_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Class Types', 'taxonomy general name' ),
		'singular_name'              => _x( 'Class Type', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Class Types' ),
		'popular_items'              => __( 'Common Class Types' ),
		'all_items'                  => __( 'All Class Types' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Class Type' ),
		'update_item'                => __( 'Update Class Type' ),
		'add_new_item'               => __( 'Add New Class Type' ),
		'new_item_name'              => __( 'New Class Type Name' ),
		'separate_items_with_commas' => __( 'Separate Class Types with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Class Type' ),
		'choose_from_most_used'      => __( 'Choose from the most common Class Type' ),
		'not_found'                  => __( 'No Class Types found.' ),
		'menu_name'                  => __( 'Class Types' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'class_type' ),
	);

	register_taxonomy( 'class_types', 'yoga_workshops', $args );
}
add_action( 'init', 'yoga_workshop_taxonomy' );

// Custom Taxonomy For Notes & Errata ==============================/
function notes_errata_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Notes & Errata', 'taxonomy general name' ),
		'singular_name'              => _x( 'Note/Errata', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Notes & Errata' ),
		'popular_items'              => null,
		'all_items'                  => __( 'All Notes & Errata' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Note/Errata' ),
		'update_item'                => __( 'Update Note/Errata' ),
		'add_new_item'               => null,
		'new_item_name'              => __( 'New Class Note/Errata' ),
		'separate_items_with_commas' => __( 'Separate with commas' ),
		'add_or_remove_items'        => __( 'Add or remove' ),
		'choose_from_most_used'      => null,
		'not_found'                  => __( 'No Notes & Errata found.' ),
		'menu_name'                  => __( 'Notes & Errata' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'notes_errata' ),
	);

	register_taxonomy( 'notes_errata', 'writings', $args );
}
add_action( 'init', 'notes_errata_taxonomy' );

// About Section ==============================/
function intro_widget() {

	register_sidebar( array(
	    'name' => __( 'Intro Text'),
	    'id' => 'intro_text',
	    'description' => __( 'Introduction Text', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

	register_sidebar( array(
	    'name' => __( 'Intro Writings'),
	    'id' => 'intro_writings',
	    'description' => __( 'Introduction Writings', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

	register_sidebar( array(
	    'name' => __( 'Intro Yoga'),
	    'id' => 'intro_yoga',
	    'description' => __( 'Introduction Yoga', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

	register_sidebar( array(
	    'name' => __( 'Intro Galleries'),
	    'id' => 'intro_galleries',
	    'description' => __( 'Introduction Galleries', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

	register_sidebar( array(
	    'name' => __( 'App'),
	    'id' => 'app_widget',
	    'description' => __( 'App Description', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

	register_sidebar( array(
	    'name' => __( 'Book'),
	    'id' => 'book_widget',
	    'description' => __( 'Book Description', 'mark' ),
	    'before_widget' => '',
	    'after_widget' => '',
	    'before_title' => '',
	    'after_title' => '',
	) );

}
add_action( 'widgets_init', 'intro_widget' );

// Add Order Column ==============================/
function add_new_menu_order_column($menu_order_columns) {
	$menu_order_columns['menu_order'] = "Order";
  	return $menu_order_columns;
}
add_action('manage_posts_columns', 'add_new_menu_order_column');

function show_order_column($name){

  	global $post;

  	switch ($name) {
    	case 'menu_order':
      	$order = $post->menu_order;
      	echo $order;
      	break;
   	default:
    	break;
   	}

}
add_action('manage_posts_custom_column','show_order_column');

function order_column_register_sortable ( $columns ){
  $columns['menu_order'] = 'menu_order';
  return $columns;
}
add_filter('manage_posts_sortable_columns','order_column_register_sortable');