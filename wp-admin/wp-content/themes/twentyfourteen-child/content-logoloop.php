<?php 

$quote = get_field( 'quote' );
$quotee = get_field( 'quotee' );
$quote_button = get_field( 'quote_button' ); ?>

<div class="module-title">
	<?php the_post_thumbnail( 'module-thumb' ); ?>
</div>

<blockquote class="quote-modal">
	<section>
		<div class="quote"><?php echo $quote; ?></div>
		<div class="quotee"><?php echo $quotee; ?></div>
		<div class="exit-modal"><?php echo $quote_button; ?></div>
	</section>
</blockquote>