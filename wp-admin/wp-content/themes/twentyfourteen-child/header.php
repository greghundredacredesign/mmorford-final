<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="description" content="The offical website of Mark Morford, award-winning columnist for the San Francisco Chronicle and SFGate, author of &#39;The Daring Spectacle: Adventures in Deviant Journalism&#39;, and a yoga teacher in San Francisco." />

	<meta name="keywords" content="Mark Morford, Daring Spectacle, satire, San Francisco Chronicle, SFGate, columnist, op-ed, opinion columnist, liberal columnist, humor, essay collection, hate mail, Obama, sex, sexuality, Burning Man, Morford, Apothecary, San Francisco writer, SFGate columnist, Apothecary app,  San Francisco yoga teacher, Yoga Tree, Yoga for Writers, yoga workshops, " />

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/favicon.ico" type="image/x-icon" />
	
	<!-- Apple Touch Icons -->
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/apple-touch-icon-144x144.png" />

	<!-- Windows 8 Tile Icons -->
    <meta name="msapplication-square70x70logo" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/largetile.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/largetile.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/largetile.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon/largetile.png" />

	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

	<!-- Fonts -->
	<script>
	  (function(d) {
	    var config = {
	      kitId: 'ahs4xnh',
	      scriptTimeout: 3000
	    },
	    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  		})(document);
	</script>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="header-bg">

		<header>

			<a href="/" class="logo">
				<?php include('img/logo.svg'); ?>
			</a>

			<button class="navicon">|||</button>

			<nav class="topnav">
				<ul>
					<li><a title="yoga_workshops" href="/yoga_workshops/" class="topnav-link topnav-yoga">Yoga</a></li>
					<li><a title="writings" href="/writings_page/" class="topnav-link topnav-writings">Writings</a></li>
					<li><a title="galleries" href="/galleries_page/" class="topnav-link topnav-galleries">Galleries</a></li>
					<li><a href="/about_page/">About</a></li>
				</ul>
			</nav>

			<form role="search" method="get" class="search-form" action="/">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="" value="" name="s" title="Search for:">
				</label>
				<input type="submit" class="search-submit" value="Search">
			</form>

			<div class="fb-like" data-href="https://www.facebook.com/markmorfordyes" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

		</header>

	</div>