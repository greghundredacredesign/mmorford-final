<?php get_header(); ?>

	<div class="main">

		<section class="span12 isotope">

			<?php 

				get_template_part( 'content', 'sidebar' );
					
				$headlineArgs = array(
					'post_type' => array( 'writings', 'yoga_workshops', 'galleries' ),
					'meta_key' => 'headline_slider',
					'meta_value' => '1'
				);
				
				query_posts( $headlineArgs );
				
				// Latest Posts Loop
				while ( have_posts() ) : the_post(); 

					get_template_part( 'content', 'latest' );	

				endwhile;
				wp_reset_query();

				// Main Loop
				while ( have_posts() ) : the_post();
				
					get_template_part( 'content', 'homeloop' );

				endwhile;

				// Logo Loop
				$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

				$logoArgs = array(
					'post_type' => array( 'logos' ),
					'posts_per_page' => 1,
					'paged' => $paged,
 					'orderby' => 'rand'
				);

				query_posts( $logoArgs );

				while ( have_posts() ) : the_post(); ?>

					<?php if ( $paged == 1 ) : ?>
						<article class="span4 module mainloop logo-module writings yoga_workshops">
					<?php else : ?>
						<article class="span4 module mainloop logo-module">
					<?php endif; ?>

						<?php get_template_part( 'content', 'logoloop' ); ?>
						
					</article>
				
				<?php endwhile; ?>
				<?php wp_reset_query();

			?>

			<section class="pagination-container mainloop">
				<div class="nav-next"><?php next_posts_link( ' ' ); ?></div>
			</section>

			<?php wp_reset_postdata(); ?>

		</section>

	</div>

<?php get_footer();