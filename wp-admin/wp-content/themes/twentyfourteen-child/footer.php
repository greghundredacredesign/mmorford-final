<div class="footer-bg">

	<footer class="single-content">

		<div class="span5">

			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup" class="footer-subscribe">
				<h3>GET ON THE LIST</h3>
				<form action="//markmorford.us8.list-manage.com/subscribe/post?u=de50be1fee1468a27da5fa7f6&amp;id=c7c278fee9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				
					<div class="mc-field-group">
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Just put in your email">
						<input type="submit" value="ok" name="subscribe" id="mc-embedded-subscribe">
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    <div style="position: absolute; left: -5000px;"><input type="text" name="b_de50be1fee1468a27da5fa7f6_c7c278fee9" tabindex="-1" value=""></div>

				</form>
			</div>
			<!--End mc_embed_signup-->
			
		</div>

		<div class="span7">
		
			<div class="span9 details">
				Indeed pretty much everything here is Copyright ©<?php echo date("Y"); ?> Mark Morford/Rapture Machine, Inc. because obviously

				<span>Website By <a href="http://chrisfettin.com/" target="_blank">Chris Fettin design</a></span>
			</div>

			<a href="http://pollardld.com" class="dp" title="david pollard">development by david pollard</a>
		</div>

	</footer>

</div>

<?php wp_footer(); ?>
</body>
</html>