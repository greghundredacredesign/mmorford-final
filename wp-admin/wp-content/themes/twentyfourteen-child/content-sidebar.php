<aside class="sidebar span4 module yoga_workshops galleries writings">

	<section class="intro">
		<?php if ( dynamic_sidebar('intro_text') ) : else : endif; ?>
	</section>

	<section class="intro writings display-none">
		<?php if ( dynamic_sidebar('intro_writings') ) : else : endif; ?>
	</section>

	<section class="intro yoga_workshops display-none">
		<?php if ( dynamic_sidebar('intro_yoga') ) : else : endif; ?>
	</section>

	<section class="intro galleries display-none">
		<?php if ( dynamic_sidebar('intro_galleries') ) : else : endif; ?>
	</section>

	<div class="social yoga_workshops galleries writings">
		<a href="mailto:etc@markmorford.com" class="social-link" target="_blank"><i class="icon-mail"></i></a>
		<a href="https://twitter.com/markmorford" class="social-link" target="_blank"><i class="icon-twitter"></i></a>
		<a href="https://www.facebook.com/markmorfordyes" class="social-link" target="_blank"><i class="icon-facebook"></i></a>
		<a href="http://instagram.com/markmorford" class="social-link" target="_blank"><i class="icon-instagram"></i></a>
	</div>

	<section class="intro writings intro-book display-none">
		<?php if ( dynamic_sidebar('book_widget') ) : else : endif; ?>
	</section>

	<section class="intro writings intro-app display-none">
		<?php if ( dynamic_sidebar('app_widget') ) : else : endif; ?>
	</section>

	<section class="classes yoga_workshops">

		<h2>Weekly Yoga Classes</h2>

		<?php 
			$yogaClassesArgs = array(
				'post_type' => 'yoga_workshops',
				'posts_per_page' => 10,
				'tax_query' => array(
					array(
						'taxonomy' => 'class_types',
						'field' => 'slug',
						'terms' => array( 'yoga_class' )
					)
				)
			);

			query_posts( $yogaClassesArgs );
			
			while ( have_posts() ) : the_post(); 

				$yogaDay = get_field( 'yoga_day_time' );
				$yogaLocation = get_field( 'yoga_location' );
				$yogaLevel = get_field( 'yoga_level' );
				$yogaUrl = get_field( 'yoga_url' ); 
			?>

				<div class="sidebar-class">
 
					<?php if( !empty($yogaDay) ): ?>	 
						<a href="<?php echo $yogaUrl; ?>" target="_blank"><h4><?php echo $yogaDay; ?></h4></a>
					<?php endif; ?>

					<?php if( !empty($yogaLevel) ): ?>	 
						<a href="<?php echo $yogaUrl; ?>" target="_blank"><span class="level"><?php echo $yogaLevel; ?></span></a>&nbsp;&nbsp;//
					<?php endif; ?>

					<?php if( !empty($yogaLocation) ): ?>	 
						<a href="<?php echo $yogaUrl; ?>" target="_blank"><span class="location"><?php echo $yogaLocation; ?></span></a>
					<?php endif; ?>

				</div>

			<?php endwhile;

			wp_reset_query();
		?>

	</section>

	<section class="classes yoga_workshops">
		<h2>Upcoming Events</h2>

		<?php 
			$workshopRetreatsArgs = array(
				'post_type' => 'yoga_workshops',
				'posts_per_page' => 10,
				'tax_query' => array(
					array(
						'taxonomy' => 'class_types',
						'field' => 'slug',
						'terms' => array( 'workshop', 'retreat', 'training', 'events' )
					)
				)
			);

			query_posts( $workshopRetreatsArgs );
			
			while ( have_posts() ) : the_post();

				$workshopLocation = get_field( 'workshop_location' );
				$workshopDate = DateTime::createFromFormat('Ymd', get_field('workshop_date')); ?>

				<div class="sidebar-class">
 
					<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>

					<?php if( !empty($workshopDate) ): ?>	 
						<a href="<?php the_permalink(); ?>"><span class="level"><?php echo $workshopDate->format('M d, Y'); ?></span></a>
					<?php endif; ?>

					<?php if( !empty($workshopLocation) ): ?>	 
						&nbsp;&nbsp;|&nbsp;<a href="<?php the_permalink(); ?>"><span class="location"><?php echo $workshopLocation; ?></span></a>
					<?php endif; ?>

				</div>

			<?php endwhile;

			wp_reset_query(); ?>

	</section>

	<!-- Begin MailChimp Signup Form -->
	<div id="mc_embed_signup" class="subscribe">
		<h3>GET ON THE LIST</h3>
		<form action="//markmorford.us8.list-manage.com/subscribe/post?u=de50be1fee1468a27da5fa7f6&amp;id=c7c278fee9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		
			<div class="mc-field-group">
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Just put in your email">
				<input type="submit" value="ok" name="subscribe" id="mc-embedded-subscribe">
			</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;"><input type="text" name="b_de50be1fee1468a27da5fa7f6_c7c278fee9" tabindex="-1" value=""></div>

		</form>
	</div>
	<!--End mc_embed_signup-->

	<section class="notes-errata-sidebar writings">
		<a href="/notes_errata/notes-errata/" class="notes-errata-link module-icon" title="notes-errata"><span>NOTES &amp; ERRATA</span>Column Archive</a>
	</section>
	
</aside>