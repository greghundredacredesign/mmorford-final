<?php 
/*
* Template Name: Galleries
*/
get_header(); 

while ( have_posts() ) : the_post(); ?>

	<section class="page-bg">
		
		<?php 
			$bgimage = get_field('background_image');
	 
			if( !empty($bgimage) ): ?>
				 
				<img src="<?php echo $bgimage['url']; ?>" alt="<?php echo $bgimage['alt']; ?>" />
				 
			<?php endif; ?>

	</section>

	<div class="main">

		<?php the_post_thumbnail( 'featured-image' ); ?>

		<div class="isotope span12">

			<?php 

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$args = array(
				'post_type' => 'galleries',
				'posts_per_page' => 24,
			   	'paged' => $paged
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) : ?>
				
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			 
					<article class="span4 module writing-five galleries">

						<a href="/galleries_page/" class="module-icon galleries-icon">
							<?php include ( 'img/gallery.svg'); ?>
						</a>
				
						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
						<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
						<div class="date-publisher"><?php the_date(); ?></div>
				
					</article>

				<?php endwhile; ?>

				<section class="pagination-container">
					<div class="nav-next"><?php next_posts_link( ' ' ); ?></div>
					<div class="nav-prev"><?php previous_posts_link( ' ' ); ?></div>
				</section>

			<?php endif; ?>

		</div>

	</div>


<?php endwhile;

get_footer();