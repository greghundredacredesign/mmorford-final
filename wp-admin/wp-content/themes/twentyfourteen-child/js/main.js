/*  =====================================
	Mobile Header
=====================================  */
function openNavicon() {
	if ( !jQuery( '.topnav' ).hasClass( 'nav_open' ) ) {
		jQuery( '.topnav').addClass( 'nav_open' );
	} else {
		jQuery( '.topnav').removeClass( 'nav_open' );
	}
}

// Open Nav Tap
jQuery( '.navicon' ).on( 'click', openNavicon );

/*  =====================================
    Waypoints
=====================================  */
jQuery( '.header-bg' ).waypoint( 'sticky', { offset : -10 });

/*  =====================================
	Masonry Layout
=====================================  */
var $isotopeContainer = jQuery('.isotope');

$isotopeContainer.isotope({
  itemSelector : '.module'
});

// Infinite Scroll
$isotopeContainer.infinitescroll({
    navSelector  : '.nav-next',    // selector for the paged navigation 
    nextSelector : '.nav-next a',  // selector for the NEXT link (to page 2)
    itemSelector : '.mainloop',     // selector for all items you'll retrieve
    loading : {
      finishedMsg : 'No more pages to load.',
      img : 'data:image/svg+xml;charset=US-ASCII,%20%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%20height%3D%2280%22%20width%3D%22100%22%20viewBox%3D%220%200%2080%20100%22%3E%0D%0A%3Cg%3E%0D%0A%09%3Cellipse%20cx%3D%2234.5%22%20cy%3D%22101.3%22%20rx%3D%224.7%22%20ry%3D%224.4%22%20style%3D%22fill%3Argba%28255%2C255%2C255%2C0.6%29%22%3E%0D%0A%09%09%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22cy%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%22101.3%22%20%0D%0A%09%09%09to%3D%220%22%0D%0A%09%09%09begin%3D%220%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%0D%0A%09%09/%3E%0D%0A%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22ry%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%220%22%20%0D%0A%09%09%09to%3D%2218%22%0D%0A%09%09%09begin%3D%220%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%20%0D%0A%09%09/%3E%0D%0A%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22rx%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%220%22%20%0D%0A%09%09%09to%3D%2225%22%0D%0A%09%09%09begin%3D%220%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%0D%0A%09%09/%3E%0D%0A%0D%0A%09%3C/ellipse%3E%0D%0A%0D%0A%09%3Cellipse%20cx%3D%2233.5%22%20cy%3D%22101.3%22%20rx%3D%220%22%20ry%3D%220%22%20style%3D%22fill%3Argba%28255%2C255%2C255%2C0.6%29%22%3E%0D%0A%09%09%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22cy%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%22101.3%22%20%0D%0A%09%09%09to%3D%220%22%20%0D%0A%09%09%09begin%3D%221.5s%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%0D%0A%09%09/%3E%0D%0A%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22ry%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%220%22%20%0D%0A%09%09%09to%3D%2218%22%0D%0A%09%09%09begin%3D%221.5s%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%20%0D%0A%09%09/%3E%0D%0A%0D%0A%09%09%3Canimate%0D%0A%09%09%09attributeName%3D%22rx%22%20%0D%0A%09%09%09dur%3D%223s%22%20%0D%0A%09%09%09from%3D%220%22%20%0D%0A%09%09%09to%3D%2225%22%0D%0A%09%09%09begin%3D%221.5s%22%0D%0A%09%09%09repeatCount%3D%22indefinite%22%0D%0A%09%09/%3E%0D%0A%0D%0A%09%3C/ellipse%3E%0D%0A%3C/g%3E%0D%0A%3C/svg%3E'
    },
  },
  // call Isotope as a callback
  function( newElements ) {
    $isotopeContainer.isotope( 'appended', jQuery( newElements ) );
    $isotopeContainer.isotope('layout');
  }
);

// Window Load helps to prevent overlapping of modules in loop
jQuery(window).bind( 'load', function() {
  $isotopeContainer.isotope( 'layout' );
  jQuery('.module-icon').unbind( 'click' );
  jQuery('.module-icon').bind( 'click', function(e) {
    e.preventDefault();
    var post_type = jQuery(this).attr('title');
    if ( jQuery(this).hasClass( 'current' ) ) {
      $isotopeContainer.isotope({
          filter : '.module'
      });
      jQuery( '.module-icon' ).removeClass( 'current' );
      jQuery( '.topnav-link' ).removeClass( 'current' );
    } else {
      $isotopeContainer.isotope({
          filter : '.' + post_type,
      });
      jQuery( this ).addClass( 'current' );
      jQuery( '.topnav-link.topnav-' + topnav_post_type ).addClass( 'current' );
    }
  });
});

jQuery(document).ajaxComplete( function() {
  jQuery('.module-icon').unbind( 'click' );
  jQuery('.module-icon').bind( 'click', function(e) {
    e.preventDefault();
    if ( jQuery(this).hasClass( 'current' ) ) {
      $isotopeContainer.isotope({
          filter : '.module'
      });
      jQuery( '.module-icon' ).removeClass( 'current' );
    } else {
      var post_type = jQuery(this).attr('title');
      $isotopeContainer.isotope({
          filter : '.' + post_type,
      });
      jQuery( '.module-icon' ).removeClass( 'current' );
      jQuery( '.module-icon' ).addClass( 'current' );
    }
  });

});

jQuery('.topnav-link').on( 'click', function(e) {
  e.preventDefault();
  var topnav_post_type = jQuery(this).attr('title');
  if ( jQuery(this).hasClass( 'current' ) ) {
      document.cookie = "_a_post_type= ; path=/";
      // Get tag slug from title attirbute
      jQuery( '.topnav-link' ).removeClass( 'current' );
      jQuery( '.module-icon' ).removeClass( 'current' );
      jQuery( '.' + topnav_post_type ).removeClass( 'current' );
      jQuery(this).addClass( 'nohover' );
      jQuery( '.intro' ).removeClass( 'display-none' );
      jQuery( '.intro.writings' ).addClass( 'display-none' );
      jQuery( '.intro.yoga_workshops' ).addClass( 'display-none' );
      jQuery( '.intro.galleries' ).addClass( 'display-none' );
      jQuery( '.subscribe' ).removeClass( 'display-none' ); 
      jQuery( 'aside .classes' ).removeClass( 'display-none' );
      jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );
      $isotopeContainer.isotope({
          filter : '.module'
      });
  } else {
    if ( topnav_post_type == 'yoga_workshops' ) {
    
      jQuery( 'aside .classes' ).removeClass( 'display-none' );
      jQuery( '.intro' ).addClass( 'display-none' );
      jQuery( '.intro.yoga_workshops' ).removeClass( 'display-none' );
      jQuery( '.subscribe' ).addClass( 'display-none' );
      jQuery( '.notes-errata-sidebar' ).addClass( 'display-none' );
    
    } else if ( topnav_post_type == 'writings' ) {
    
      jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );
      jQuery( '.intro' ).addClass( 'display-none' );
      jQuery( '.intro.writings' ).removeClass( 'display-none' );
      jQuery( '.subscribe' ).addClass( 'display-none' );
      jQuery( 'aside .classes' ).addClass( 'display-none' );
    
    } else if ( topnav_post_type == 'galleries' ) {
    
      jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );
      jQuery( '.intro' ).addClass( 'display-none' );
      jQuery( '.intro.galleries' ).removeClass( 'display-none' );
      jQuery( '.subscribe' ).addClass( 'display-none' );
      jQuery( 'aside .classes' ).addClass( 'display-none' );
      jQuery( '.notes-errata-sidebar' ).addClass( 'display-none' );
    
    } else {
    
      jQuery( '.intro' ).removeClass( 'display-none' );
      jQuery( '.intro.writings' ).addClass( 'display-none' );
      jQuery( '.intro.yoga_workshops' ).addClass( 'display-none' );
      jQuery( '.intro.galleries' ).addClass( 'display-none' );
      jQuery( '.subscribe' ).removeClass( 'display-none' ); 
      jQuery( 'aside .classes' ).removeClass( 'display-none' );
      jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );
    } 

    $isotopeContainer.isotope({
      filter : '.' + topnav_post_type
    });

    $isotopeContainer.infinitescroll( 'retrieve' );

    jQuery( '.topnav-link' ).removeClass( 'current' );
    jQuery( '.module-icon' ).removeClass( 'current' );
    jQuery( this ).addClass( 'current' );
    jQuery( '.module-icon.' + topnav_post_type + '-icon' ).addClass( 'current' );
    jQuery( '.topnav-link' ).removeClass( 'nohover' );
  }
});
	
// Zoom ===========================================
jQuery('.single-gallery').magnificPopup({
	delegate: '.gallery-image',
	type: 'image',
	gallery: {
		enabled: true,
		navigateByImgClick: true,
		tPrev: 'Previous (Left arrow key)',
		tNext: 'Next (Right arrow key)',
  },
  image: {
    titleSrc: 'data-caption'
  }
});

// logo popup =====================================
jQuery( '.main' ).on( 'click', '.logo-module', function() {
  jQuery(window).width();
  jQuery(this).toggleClass( 'show-modal' );
});


// Classes List Open ==============================
jQuery( '.classes' ).on( 'click', function() {
  jQuery( this ).toggleClass( 'hover' );
});

// Google ==============================
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-54993125-1', 'auto');
ga('send', 'pageview');

/*  =====================================
    Facebook
=====================================  */
jQuery(document).ready( function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=717875478235715&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));