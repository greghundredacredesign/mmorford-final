// Top Nav - Set cookie to filter home page
jQuery('.topnav-link').on( 'click', function(e) {
    e.preventDefault();
    var post_type = jQuery(this).attr('title');
    document.cookie = "_a_post_type=" + post_type + "; path=/";
    document.location.assign( window.location.origin );
});

// Module Icon Click - Not Home
jQuery( '.module-icon' ).on( 'click', function(e) {
    e.preventDefault();
    var post_type = jQuery(this).attr('title');
    document.cookie = "_a_post_type=" + post_type + "; path=/";
    document.location = "/";
});

// Remove Cookie On Logo Click, then go home
jQuery( '.logo' ).on( 'click', function(e) {
	e.preventDefault();      
	document.cookie = "_a_post_type= ; path=/";
	document.location = "/";
});