$slickSlider = jQuery( '.slider' );

$slickSlider.slick({
	swipe : false,
	arrows : false,
	cssEase : 'ease-in-out',
	draggable : false,
	touchMove : false,
	speed : 200,
	onBeforeChange : function() {
		jQuery(this).css( 'background', 'red' );
	}
});

jQuery(window).load( function() {
	$height = jQuery( '.slick-active' ).height();
	jQuery( '.slider' ).height( $height );
});


jQuery( '.slide-nav a:first-of-type' ).addClass( 'current' );

jQuery( '.slide-nav a' ).on( 'click', function(e) {
	
	e.preventDefault();
	
	var nav_item = jQuery( '.slide-nav a' ).index( this );

	$slickSlider.slickGoTo( nav_item );
	$slickSlider.height(jQuery('.slick-active').height())
	jQuery( '.slide-nav a' ).removeClass( 'current' );
	jQuery( this ).addClass( 'current' );

} );