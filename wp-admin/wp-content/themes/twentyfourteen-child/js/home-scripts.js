// Setting Filter if cookie is set
// Cookie is set on the about page
jQuery(document).ready( function() {

    var cookies = document.cookie.split(';');

    for ( var i = 0; i < cookies.length; i++ ) {
        
        var cookie = cookies[i].split("=");

        if ( cookie[0] == '_a_post_type' || cookie[1] !== undefined ) {
            
            var type = cookie[1];

            if ( type == 'galleries' ) {
                
                $isotopeContainer.isotope({
                    filter : '.galleries'
                });

                jQuery( '.topnav a[title=galleries]' ).addClass( 'current' );
                jQuery( '.intro' ).addClass( 'display-none' );
                jQuery( '.intro.galleries' ).removeClass( 'display-none' );
                jQuery( '.subscribe' ).addClass( 'display-none' ); 
                jQuery( 'aside .classes' ).addClass( 'display-none' );
                jQuery( '.notes-errata-sidebar' ).addClass( 'display-none' );

            } else if ( type == 'writings' ) {
                
                $isotopeContainer.isotope({
                    filter : '.writings'
                });

                jQuery( '.topnav a[title=writings]' ).addClass( 'current' );
                jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );
                jQuery( '.intro' ).addClass( 'display-none' );
                jQuery( '.intro.writings' ).removeClass( 'display-none' );
                jQuery( '.subscribe' ).addClass( 'display-none' );
                jQuery( 'aside .classes' ).addClass( 'display-none' );

            } else if ( type == 'yoga_workshops' ) {

                $isotopeContainer.isotope({
                    filter : '.yoga_workshops'
                });
                
                jQuery( '.topnav a[title=yoga_workshops]' ).addClass( 'current' );
                jQuery( 'aside .classes' ).removeClass( 'display-none' );
                jQuery( '.intro' ).addClass( 'display-none' );
                jQuery( '.intro.yoga_workshops' ).removeClass( 'display-none' );
                jQuery( '.subscribe' ).addClass( 'display-none' );
                jQuery( '.notes-errata-sidebar' ).addClass( 'display-none' );

            }
        }
    }
});

// Remove Cookie and Filter Posts on Logo Click
jQuery( '.logo' ).on( 'click', function(e) {
    e.preventDefault();      

    document.cookie = "_a_post_type= ; path=/";

    jQuery( '.topnav-link' ).removeClass( 'current' );
    jQuery( '.module-icon' ).removeClass( 'current' );
    jQuery( '.intro' ).removeClass( 'display-none' );
    jQuery( '.intro.writings' ).addClass( 'display-none' );
    jQuery( '.intro.yoga_workshops' ).addClass( 'display-none' );
    jQuery( '.intro.galleries' ).addClass( 'display-none' );
    jQuery( '.subscribe' ).removeClass( 'display-none' ); 
    jQuery( 'aside .classes' ).removeClass( 'display-none' );
    jQuery( '.notes-errata-sidebar' ).removeClass( 'display-none' );

    $isotopeContainer.isotope({
      filter : '.module'
    });
});