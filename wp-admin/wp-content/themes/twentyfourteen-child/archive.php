<?php get_header(); ?>

	<div class="main">

		<section class="span12 isotope">

			<?php get_template_part( 'content', 'sidebar' ); ?>
									
			<?php while ( have_posts() ) : the_post();

				$thePostType = get_post_type( get_the_ID() );
		
				$width = get_field( 'span_two_columns' );

				if ( $width == true ) {
					$double = 'span8';
				} else {
					$double = '';
				}
				
				if ( $thePostType == 'galleries') : ?>

					<article class="span4 module mainloop galleries <?php echo $double; ?>">
						
						<a href="/galleries_page/" class="module-icon galleries-icon" title="galleries"><?php include ( 'img/gallery.svg'); ?></a>
						
						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

						<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
						
						<div class="date-publisher"><?php the_date(); ?></div>
					
					</article>

				<?php elseif ( $thePostType == 'writings' ) :

					$postStyle = get_field('post_style'); 
					
					if ( $postStyle == 'One' ): ?>
						<article class="span4 module mainloop writing writing-one <?php echo $double; ?>">
					<?php elseif ( $postStyle == 'Two' ) : ?>
						<article class="span4 module mainloop writing writing-two <?php echo $double; ?>">
					<?php elseif ( $postStyle == 'Three' ) : ?>
						<article class="span4 module mainloop writing writing-three <?php echo $double; ?>">
					<?php elseif ( $postStyle == 'Four' ) : ?>
						<article class="span4 module mainloop writing writing-four <?php echo $double; ?>">
					<?php endif; ?>

						<a href="/writings_page/" class="module-icon writing-icon" title="writing"><?php include ( 'img/writing.svg'); ?></a>
						
						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

						<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
						
						<div class="date-publisher"><?php the_date(); ?> | <?php the_terms( $post->ID, 'publishers', '', ' ', '' ); ?></div>
			
					</article>

				<?php elseif ( $thePostType == 'yoga_workshops' ) : ?>

					<article class="span4 module mainloop yoga <?php echo $double; ?>">
						
						<div class="einstein">

							<a href="/yoga/" class="module-icon yoga-icon" title="yoga"><?php include ( 'img/yoga.svg'); ?></a>
						
							<div class="module-thumb">
								<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
							</div>
							
							<a href="<?php the_permalink(); ?>" class="module-title">
								<?php if ( has_term( 'workshop', 'class_types' ) || has_term( 'retreat', 'class_types' ) ) : ?>
									New Workshop:
								<?php endif; ?>

								<?php the_title(); ?>
							</a>
						
							<div class="excerpt"><?php the_excerpt(); ?></div>

						</div>
					
					</article>

				<?php elseif ( $thePostType == 'social_hashtag' ) : ?>

					<article class="span4 module mainloop instagram <?php echo $double; ?>">
						
						<a href="/instagram/" class="module-icon instagram-icon" title="instagram" rel="nofollow" target="_blank"><i class="icon-instagram">&nbsp;</i></a>

						<?php $imgUrl = get_post_meta( $post->ID, 'social_hashtag_full_url', true ); ?>

						<div class="module-thumb">
							<a href="<?php echo $imgUrl ?>" target="_blank"><img src="<?php echo $imgUrl ?>" /></a>
						</div>
						
						<div class="excerpt"><?php the_title(); ?></div>	

					</article>
				
				<?php elseif ( $thePostType == 'post' ) : ?>

					<?php if ( in_category( 'twitter' ) ) : ?>

						<article class="span4 module mainloop twitter <?php echo $double; ?>">
							<?php the_content(); ?>
						</article>

					<?php elseif ( has_post_format( 'video' ) ) : ?>

						<article class="span4 module mainloop video <?php echo $double; ?>">
							
							<a href="<?php the_permalink(); ?>" class="module-icon video-icon" title="video"><?php include ( 'img/video.svg'); ?></a>
						
							<?php the_content(); ?>
							
							<?php 
								$videoCaption = get_field('video_caption');
								if ( $videoCaption ) {
									echo $videoCaption;
								}; ?>

						</article>

					<?php else : ?>

						<article class="span4 module mainloop <?php echo $double; ?>">
							
							<div class="module-thumb">
								<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
							</div>

							<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
							
							<div class="excerpt"><?php the_excerpt(); ?></div>
						
						</article>

					<?php endif; ?>

				<?php else : ?>

					<article class="span4 module mainloop <?php echo $double; ?>">
						<?php the_title(); ?>
						<?php the_content(); ?>
					</article>

				<?php endif; ?>

			<?php endwhile; ?>
						
		</section>
		
	</div>

<?php get_footer();