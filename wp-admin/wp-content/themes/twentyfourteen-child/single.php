<?php get_header(); ?>

	<div class="main spread">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="single">

				<?php if ( has_post_thumbnail() ) : ?>

					<div class="single-image">
						<?php the_post_thumbnail( 'hero-thumb' ); ?>
					</div>

					<div class="single-title-pattern">
						<h1 class="single-title"><?php the_title(); ?></h1>
					</div>
				
				<?php else : ?>

					<div class="single-content">
						<h1 class="single-title"><?php the_title(); ?></h1>
					</div>

				<?php endif; ?>

				<div class="single-content">
					
					<?php the_content(); ?>

				</div>

			</div>

		<?php endwhile; ?>

	</div>

<?php get_footer();
