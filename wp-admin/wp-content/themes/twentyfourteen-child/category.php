<?php get_header(); ?>

	<div class="main">

		<section class="span12 isotope">

			<?php get_template_part( 'content', 'sidebar' ); ?>
									
			<?php while ( have_posts() ) : the_post();

				get_template_part( 'content', 'homeloop' );

			endwhile; ?>
						
		</section>

	</div>

<?php get_footer();