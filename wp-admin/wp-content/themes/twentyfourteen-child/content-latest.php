<?php 

$thePostType = get_post_type( get_the_ID() );

$width = get_field( 'span_two_columns' );
$yogaGallery = get_field( 'yoga_gallery' );

if ( $width == true ) {
	$span = 'span8';
} else {
	$span = 'span4';
}

if ( $yogaGallery == true ) {
	$showYogaGallery = 'yoga_workshops';
} else {
	$showYogaGallery = '';
}

// Get the style
$postStyle = get_field('post_style'); 

// is it a notes-errata post
if ( has_term( 'notes-errata', 'notes_errata' ) ) {
	$notesErrataClass = 'notes-errata';
} else {
	$notesErrataClass = '';
}

// Loop through the options
if ( $postStyle == 'One' ): ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module writing-one">
<?php elseif ( $postStyle == 'Two' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module writing-two">
<?php elseif ( $postStyle == 'Three' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module writing-three">
<?php elseif ( $postStyle == 'Four' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module writing-four">
<?php elseif ( $postStyle == 'Five' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module writing-five">
<?php elseif ( $postStyle == 'Six' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module moduleone">
<?php elseif ( $postStyle == 'Seven' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module moduletwo">
<?php elseif ( $postStyle == 'Eight' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module modulethree">
<?php elseif ( $postStyle == 'Nine' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module modulefour">
<?php elseif ( $postStyle == 'Ten' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module modulefive">
<?php elseif ( $postStyle == 'Eleven' ) : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module modulesix">
<?php else : ?>
	<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module moduleone">
<?php endif; ?>

<?php if ( $thePostType == 'galleries') : ?>
	
	<a href="/galleries_page/" class="module-icon new-stuff-icon" title="galleries">LATEST</a>
	<a href="/galleries_page/" class="module-icon icon-description" title="galleries">Gallery</a>
	
	<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

	<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

	<div class="date-publisher"><?php the_date(); ?></div>

<?php elseif ( $thePostType == 'writings' ) : ?>

	<a href="/writings_page/" class="module-icon new-stuff-icon" title="writings">LATEST</a>

	<?php if ( has_term( 'notes-errata', 'notes_errata' ) ) : ?>

		<a href="/writings_page/" class="module-icon icon-description" title="writings">Notes &amp; Errata</a>

	<?php else : ?>

		<a href="/writings_page/" class="module-icon icon-description" title="writings">Other Writings</a>

	<?php endif; ?>
	
	<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

	<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

	<div class="date-publisher"><?php the_date(); ?> <?php the_terms( $post->ID, 'publishers', ' | ', ' | ', '' ); ?></div>

<?php elseif ( $thePostType == 'yoga_workshops' ) : ?>

	<?php if ( ! has_term( 'yoga_class', 'class_types' ) ) : ?>
	
		<a href="/yoga/" class="module-icon new-stuff-icon" title="yoga_workshops">LATEST</a>

		<?php if ( has_term( 'retreat', 'class_types' ) ) : ?>

			<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Retreat</a>

		<?php elseif ( has_term( 'workshop', 'class_types' ) ) : ?>

			<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Workshop</a>

		<?php elseif ( has_term( 'training', 'class_types' ) ) : ?>

			<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Training</a>

		<?php elseif ( has_term( 'events', 'class_types' ) ) : ?>

			<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Event</a>

		<?php else : ?>

			<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">More Yoga</a>

		<?php endif; ?>

		<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

		<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

		<?php
			$workshopDate = DateTime::createFromFormat('Ymd', get_field('workshop_date'));
		 	
		 	if( !empty($workshopDate) ): ?>	 
				<div class="date-publisher">
					<a href="<?php the_permalink(); ?>"><?php echo $workshopDate->format('M d, Y'); ?></a>
				</div>
			
			<?php endif; ?>

		<div class="excerpt"><?php the_excerpt(); ?></div>
	
	<?php endif; ?>

<?php endif; ?>

</article>