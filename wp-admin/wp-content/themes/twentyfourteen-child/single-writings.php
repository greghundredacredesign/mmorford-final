<?php get_header(); ?>

	<div class="main spread">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php $postStyle = get_field('post_style'); ?>

			<div class="single-writing <?php echo $postStyle; ?>">

				<?php if ( has_post_thumbnail() ) : ?>

					<div class="single-image">
						<?php the_post_thumbnail( 'hero-thumb' ); ?>
					</div>
					
					<div class="single-title-pattern">
						<h1 class="single-title"><?php the_title(); ?></h1>
					</div>

				<?php else : ?>

					<div class="single-content">
						<h1 class="single-title-sans-image"><?php the_title(); ?></h1>
					</div>

				<?php endif; ?>

				<div class="single-content taxonomy-content">
					
					<?php print apply_filters( 'taxonomy-images-list-the-terms', '', array( 'image_size' => 'thumb', 'taxonomy' => 'publishers' ) ); ?>
				
				</div>

				<div class="byline">
					
					<?php $byline = get_field( 'byline_author' ); ?>
					
					<div class="author">
						<?php if( $byline ) : ?>
							Posted by: <?php echo $byline; ?> on 
						<?php endif; ?>
						<?php the_date(); ?>
					</div>

					<?php 
						$jsps_networks = array( 'twitter', 'facebook' );
						juiz_sps( $jsps_networks ); ?>
				
				</div>

				<div class="single-content droppin-cappy">
					
					<?php the_content(); ?>

					<div class="signoff">
						<?php include( 'img/signoff.svg' ); ?>
					</div>

					<div class="copywrite">
						<em>Copyright © <?php echo date("Y"); ?> Mark Morford | mean who else could it be, really. Ask before reposting. Seriously.</em>
					</div>

				</div>

			</div>

		<?php endwhile; ?>

	</div>

<?php get_footer();
