<?php 
/*
* Template Name: Writings Page
*/
get_header(); ?>

	<section class="page-bg">
		
		<?php 
			$bgimage = get_field('background_image');
	 
			if( !empty($bgimage) ): ?>
				 
				<img src="<?php echo $bgimage['url']; ?>" alt="<?php echo $bgimage['alt']; ?>" />
				 
			<?php endif; ?>

	</section>

	<div class="main">

		<?php the_post_thumbnail( 'featured-image' ); ?>

		<div class="isotope span12">

		<?php 

		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		$args = array(
			'post_type' => 'writings',
			'posts_per_page' => 24,
		   	'paged' => $paged
		);

		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ) : ?>
			
			<?php while ( $the_query->have_posts() ) : $the_query->the_post();

				$postStyle = get_field('post_style');
		 
				if ( $postStyle == 'One' ): ?>
					<article class="span4 module writing writing-one">
				<?php elseif ( $postStyle == 'Two' ) : ?>
					<article class="span4 module writing writing-two">
				<?php elseif ( $postStyle == 'Three' ) : ?>
					<article class="span4 module writing writing-three">
				<?php elseif ( $postStyle == 'Four' ) : ?>
					<article class="span4 module writing writing-four">
				<?php endif; ?>

						<a href="/writings_page/" class="module-icon writing-icon">
							<?php include ( 'img/writing.svg'); ?>
						</a>
				
						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
						<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
						<div class="date-publisher"><?php the_date(); ?> | <?php the_terms( $post->ID, 'publishers', '', ' ', '' ); ?></div>
				
					</article>

			<?php endwhile; ?>

			<section class="pagination-container">
				<div class="nav-next"><?php next_posts_link( ' ' ); ?></div>
				<div class="nav-prev"><?php previous_posts_link( ' ' ); ?></div>
			</section>

		<?php endif; ?>

		</div>

	</div>

<?php get_footer();