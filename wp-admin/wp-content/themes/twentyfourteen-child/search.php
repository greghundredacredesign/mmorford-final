<?php get_header(); ?>
	<script>
		jQuery('.topnav-link').on( 'click', function(e) {
		    e.preventDefault();
		    var post_type = jQuery(this).attr('title');
		    document.cookie = "_a_post_type=" + post_type + "; path=/";
		    document.location.assign( window.location.origin );
		});
	</script>
	
	<div class="main">

		<h5><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h5>

		<section class="span12 isotope">

			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post();

					get_template_part( 'content', 'homeloop' );

				endwhile;

			else : ?>

				<article class="span12 module mainloop negative-search">

					<h1>No Results. Keep Searching:</h1>

					<form role="search" method="get" class="search-page-search" action="/">
						<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-page-search-field" placeholder="" value="" name="s" title="Search for:">
						</label>
						<input type="submit" class="search-page-search-submit" value="Search">
					</form>

				</article>

				<?php wp_reset_postdata(); ?>
	
			<?php endif; ?>

		</section>

		<section class="pagination-container">
			<div class="nav-next"><?php next_posts_link( ' ' ); ?></div>
		</section>

	</div>

<?php get_footer();