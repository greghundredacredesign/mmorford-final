<?php 

$thePostType = get_post_type( get_the_ID() );

$width = get_field( 'span_two_columns' );
$yogaGallery = get_field( 'yoga_gallery' );

// Get the span
if ( $width === true ) {
	$span = 'span8';
} else {
	$span = 'span4';
}

if ( $yogaGallery == true ) {
	$showYogaGallery = 'yoga_workshops';
} else {
	$showYogaGallery = '';
}

// Get the style
$postStyle = get_field('post_style'); 

// is it a notes-errata post
if ( has_term( 'notes-errata', 'notes_errata' ) ) {
	$notesErrataClass = 'notes-errata';
} else {
	$notesErrataClass = '';
}

if ( $thePostType == 'galleries' || $thePostType == 'writings' || $thePostType == 'yoga_workshops' ) :

	// Loop through the options
	if ( $postStyle == 'One' ): ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop writing-one">
	<?php elseif ( $postStyle == 'Two' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop writing-two">
	<?php elseif ( $postStyle == 'Three' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop writing-three">
	<?php elseif ( $postStyle == 'Four' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop writing-four">
	<?php elseif ( $postStyle == 'Five' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop writing-five">
	<?php elseif ( $postStyle == 'Six' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop moduleone">
	<?php elseif ( $postStyle == 'Seven' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop moduletwo">
	<?php elseif ( $postStyle == 'Eight' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop modulethree">
	<?php elseif ( $postStyle == 'Nine' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop modulefour">
	<?php elseif ( $postStyle == 'Ten' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop modulefive">
	<?php elseif ( $postStyle == 'Eleven' ) : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop modulesix">
	<?php else : ?>
		<article class="<?php echo $span; ?> <?php echo $thePostType; ?> <?php echo $notesErrataClass; ?> <?php echo $showYogaGallery; ?> module mainloop moduleone">
	<?php endif; ?>

	<?php if ( $thePostType == 'galleries') : ?>
		
		<a href="/galleries_page/" class="module-icon galleries-icon" title="galleries"><?php include ( 'img/gallery.svg'); ?></a>
		<a href="/galleries_page/" class="module-icon icon-description" title="galleries">Gallery</a>

		<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

		<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

		<div class="date-publisher"><?php the_date(); ?></div>

	<?php elseif ( $thePostType == 'writings' ) : ?>

			<a href="/writings_page/" class="module-icon writing-icon" title="writings"><?php include ( 'img/writing.svg'); ?></a>

			<?php if ( has_term( 'notes-errata', 'notes_errata' ) ) : ?>

				<a href="/writings_page/" class="module-icon icon-description" title="writings">Notes &amp; Errata</a>

			<?php else : ?>

				<a href="/writings_page/" class="module-icon icon-description" title="writings">Other Writings</a>

			<?php endif; ?>

			<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

			<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

			<div class="date-publisher"><?php the_date(); ?><?php the_terms( $post->ID, 'publishers', ' | ', ' | ', '' ); ?></div>

	<?php elseif ( $thePostType == 'yoga_workshops' ) : ?>

		<?php if ( ! has_term( 'yoga_class', 'class_types' ) ) : ?>

			<a href="/yoga/" class="module-icon yoga-icon" title="yoga_workshops"><?php include ( 'img/yoga.svg'); ?></a>

			<?php if ( has_term( 'retreat', 'class_types' ) ) : ?>

				<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Retreat</a>

			<?php elseif ( has_term( 'workshop', 'class_types' ) ) : ?>

				<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Workshop</a>

			<?php elseif ( has_term( 'training', 'class_types' ) ) : ?>

				<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Training</a>

			<?php elseif ( has_term( 'events', 'class_types' ) ) : ?>

				<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">Event</a>

			<?php else : ?>

				<a href="/yoga/" class="module-icon icon-description" title="yoga_workshops">More Yoga</a>

			<?php endif; ?>

			<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

			<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

			<?php
				$workshopDate = DateTime::createFromFormat('Ymd', get_field('workshop_date'));
			 	
			 	if( !empty($workshopDate) ): ?>	 
					<div class="date-publisher">
						<a href="<?php the_permalink(); ?>"><?php echo $workshopDate->format('M d, Y'); ?></a>
					</div>
				
				<?php endif; ?>

			<div class="excerpt"><?php the_excerpt(); ?></div>

		<?php endif; ?>

	<?php endif; ?>

</article>

<?php elseif ( $thePostType == 'social_hashtag' ) : ?>

	<?php if ( has_term( 'mm', 'social_hashtag_tags' ) ) : ?>

		<article class="module mainloop instagram <?php echo $span; ?>">
			
			<a href="/instagram/" class="module-icon instagram-icon" title="instagram" rel="nofollow" target="_blank"><i class="icon-instagram">&nbsp;</i></a>

			<?php 
				$imgUrl = get_post_meta( $post->ID, 'social_hashtag_thumb_url', true ); 
				$instaUrl = get_post_meta( $post->ID, 'social_hashtag_pic_link', true );
			?>

			<div class="module-thumb">
				<a href="<?php echo $instaUrl ?>" class="insta-url" target="_blank"><img src="<?php echo $imgUrl ?>" alt="instagram icon" /></a>
				<div class="date-publisher"><a href="http://instagram.com/markmorford" target="_blank">@markmorford</a> | <?php the_date(); ?></div>
			</div>
			
			<div class="excerpt">
				<?php
					$title = get_the_title(); 
					$title = substr( $title, 0, 155);
	    			echo $title; ?>
	    		<a href="<?php echo $instaUrl ?>" target="_blank" class="excerpt-link">read more »</a>
    		</div>	

		</article>

	<?php endif; ?>

<?php elseif ( $thePostType == 'post' ) : ?>

	<?php if ( in_category( 'twitter' ) ) : ?>

		<article class="module mainloop twitter <?php echo $span; ?>">
			<?php the_content(); ?>
		</article>

	<?php elseif ( has_post_format( 'video' ) ) : ?>

		<article class="module mainloop video <?php echo $span; ?>">
			
			<a href="<?php the_permalink(); ?>" class="module-icon video-icon" title="video"><?php include ( 'img/video.svg'); ?></a>
		
			<?php the_content(); ?>
			
			<?php 
				$videoCaption = get_field('video_caption');
				if ( $videoCaption ) {
					echo $videoCaption;
				}; ?>

		</article>

	<?php else : ?>

		<article class="module mainloop <?php echo $span; ?>">
			
			<div class="module-thumb">
				<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
			</div>

			<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
			
			<div class="excerpt"><?php the_excerpt(); ?></div>
		
		</article>

	<?php endif; ?>

<?php else : ?>

	<article class="module mainloop span4">
		<div class="module-thumb">
			<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>
		</div>
		<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
	</article>

<?php endif; ?>