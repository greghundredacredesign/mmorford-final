<?php 
/*
* Template Name: About
*/
get_header(); 

while ( have_posts() ) : the_post(); ?>

	<section class="page-bg">
		
		<?php 
			$bgimage = get_field('background_image');
	 
			if( !empty($bgimage) ): ?>
				 
				<img src="<?php echo $bgimage['url']; ?>" alt="<?php echo $bgimage['alt']; ?>" />
				 
			<?php endif; ?>

	</section>

	<div class="main no-margin">

		<?php the_post_thumbnail( 'featured-image' ); ?>

	</div>

<?php endwhile; ?>

<div class="main">

	<div class="slide-nav single-content">
		<a href="#yoga" id="nav_yoga">Yoga Bio</a>
		<a href="#writing" id="nav_writing">Writing Bio</a>
		<a href="#media" id="nav_media">Media Extras</a>
	</div>

	<section class="single-content slider">

	<?php 
		query_posts( 'post_type=about&orderby=title&order=DESC' );
		while ( have_posts() ) : the_post(); ?>

			<div>
				
				<?php the_content(); ?>

			</div>

	<?php 
		endwhile;
		wp_reset_query(); ?>

	</section>

</div>

<?php get_footer();