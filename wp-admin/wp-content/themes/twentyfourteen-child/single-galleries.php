<?php get_header(); ?>

	<div class="main spread">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php $postStyle = get_field('post_style'); ?>

			<div class="single-gallery <?php echo $postStyle; ?>">

				<div class="single-content">
					<h1 class="single-title"><?php the_title(); ?></h1>
				</div>

				<div class="byline">
					
					<?php $byline = get_field( 'byline_author' ); ?>
					
					<?php if( $byline ) : ?>
						<div class="author">Posted by: <?php echo $byline; ?> on <?php the_date(); ?></div>
					<?php endif; ?>

					<?php 
						$jsps_networks = array( 'twitter', 'facebook' );
						juiz_sps( $jsps_networks ); ?>
				
				</div>

				<div class="single-content">

					<article>
						<?php the_content(); ?>
					</article>
					
					<?php 
						$imgGallery = get_field( 'gallery' ); 

						if ( $imgGallery ) :

							foreach ( $imgGallery as $img ) : ?>
								
								<a href="<?php echo $img['url']; ?>" class="gallery-image" data-caption="<?php echo $img['caption']; ?>">
									<img src="<?php echo $img['sizes']['module-thumb']; ?>" alt="<?php echo $img['alt']; ?>" />
								</a>

							<?php 
							endforeach;
						
						endif;
					?>

				</div>

			</div>

		<?php endwhile;

		wp_reset_query(); ?>

		<div class="single-content">
			
			<h1>More Galleries</h1>

			<?php 

			$moreArgs = array(  
	  			'post_type' => 'galleries',
		    	'post__not_in' => array( $post->ID ),
		    	'posts_per_page' => 4
		    ); 

		    $moreGalleries = new WP_query( $moreArgs );  

		    while( $moreGalleries->have_posts() ) : $moreGalleries->the_post();  ?>

		    	<article class="span6 module writing-five galleries">
						
					<a href="/galleries_page/" class="module-icon galleries-icon" title="galleries"><?php include ( 'img/gallery.svg'); ?></a>
					
					<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

					<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
					
					<div class="date-publisher"><?php the_date(); ?></div>
					
				</article>

		    <?php endwhile;

		    wp_reset_query(); ?>

		</div>

	</div>

<?php get_footer();
