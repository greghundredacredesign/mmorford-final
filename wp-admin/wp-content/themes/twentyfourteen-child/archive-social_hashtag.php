<?php get_header(); ?>

	<div class="main spread3 social" id="social_hashtags">	

		<h1>Instagram</h1>
		
		<?php 

			global $post;

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var('paged') : 1;
			
			$args = array(
				'post_type' => 'social_hashtag',
				'posts_per_page' => 30,
				'orderby' => 'date',
				'order' => 'DESC',
				'paged' => $paged
			);

			$get_posts = new WP_Query($args);

			if( $get_posts->have_posts() ) : ?>
		
			<?php while( $get_posts->have_posts() ) : $get_posts->the_post(); ?>

				<?php
					$insta_img = get_post_meta($post->ID, 'social_hashtag_full_url', true);
					$insta_img_thumb = get_post_meta($post->ID, 'social_hashtag_thumb_url', true);
					$insta_handle = get_post_meta($post->ID, 'social_hashtag_userhandle', true);

					if ( $insta_handle == 'markmorford' ) :
				?>
		
					<div class="span4 module instagram">
						<a href="<?php echo $insta_img; ?>">
							<img src="<?php echo $insta_img_thumb; ?>" />
						</a>
					</div>

				<?php endif; ?>
				
			<?php endwhile; ?>

			<section class="pagination-container clear">
				<?php next_posts_link( '<div class="pagination-box">More</div>', $get_posts->max_num_pages ); ?>
			</section>

			<?php wp_reset_postdata(); ?>
		
		<?php else: ?>
			<h4>No Content was found</h4>
		<?php endif; ?>

	</div>

<?php get_footer();