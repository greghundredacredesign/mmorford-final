<?php get_header(); ?>

	<div class="main main-yoga">

		<section class="isotope span12">

			<?php 

			get_template_part( 'content', 'sidebar' );

			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$args = array(
				'post_type' => 'yoga_workshops',
				'posts_per_page' => 24,
			   	'paged' => $paged
			);

			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) : ?>
				
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();

					$thePostType = get_post_type( get_the_ID() );

					$width = get_field( 'span_two_columns' );
					$yogaGallery = get_field( 'yoga_gallery' );

					// Get the span
					if ( $width === true ) {
						$span = 'span8';
					} else {
						$span = 'span4';
					}

					if ( $yogaGallery == true ) {
						$showYogaGallery = 'yoga_workshops';
					} else {
						$showYogaGallery = '';
					}

					// Get the style
					$postStyle = get_field('post_style');
											
					if ( $postStyle == 'One' ): ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-one">
					<?php elseif ( $postStyle == 'Two' ) : ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-two">
					<?php elseif ( $postStyle == 'Three' ) : ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-three">
					<?php elseif ( $postStyle == 'Four' ) : ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-four">
					<?php elseif ( $postStyle == 'Five' ) : ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-five">
					<?php else : ?>
						<article class="<?php echo $span; ?> <?php echo $thePostType; ?> module mainloop writing-one">
					<?php endif; ?>

					<?php if ( ! has_term( 'yoga_class', 'class_types' ) ) : ?>

						<a href="/yoga/" class="module-icon yoga-icon" title="yoga_workshops"><?php include ( 'img/yoga.svg'); ?></a>

						<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

						<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>

						<div class="excerpt"><?php the_excerpt(); ?></div>

					<?php endif; ?>

					</article>

				<?php endwhile; ?>

				<section class="pagination-container">
					<div class="nav-next"><?php next_posts_link( ' ' ); ?></div>
					<div class="nav-prev"><?php previous_posts_link( ' ' ); ?></div>
				</section>

			<?php endif; ?>

		</section>

	</div>

<?php get_footer();