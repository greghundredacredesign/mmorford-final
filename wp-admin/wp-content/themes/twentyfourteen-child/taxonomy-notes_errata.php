<?php get_header(); ?>

	<div class="main">

		<section class="span12 isotope">

			<?php while ( have_posts() ) : the_post(); 

				$postStyle = get_field('post_style');

				$width = get_field( 'span_two_columns' );

				if ( $width == true ) {
					$double = 'span8';
				} else {
					$double = 'span4';
				}

				if ( $postStyle == 'One' ): ?>
					<article class="<?php echo $double; ?> module writing writing-one">
				<?php elseif ( $postStyle == 'Two' ) : ?>
					<article class="<?php echo $double; ?> module writing writing-two">
				<?php elseif ( $postStyle == 'Three' ) : ?>
					<article class="<?php echo $double; ?> module writing writing-three">
				<?php elseif ( $postStyle == 'Four' ) : ?>
					<article class="<?php echo $double; ?> module writing writing-four">
				<?php endif; ?>

					<span class="module-icon new-stuff-icon notes-errata-icon" title="writing">NOTES &amp; ERRATA</span>

					<a href="<?php the_permalink(); ?>" class="image-link"><?php the_post_thumbnail( 'module-thumb' ); ?></a>

					<a href="<?php the_permalink(); ?>" class="module-title"><?php the_title(); ?></a>
					
					<div class="date-publisher"><?php the_date(); ?> | <?php the_terms( $post->ID, 'publishers', '', ' ', '' ); ?></div>
		
				</article>

			<?php endwhile; ?>

		</section>

	</div>

<?php get_footer();