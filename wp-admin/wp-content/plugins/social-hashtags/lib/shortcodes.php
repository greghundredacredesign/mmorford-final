<?php

// this is under construction :)

add_shortcode('social_hashtag_pics', 'display_social_hashtag_pics');
function display_social_hashtag_pics( $atts ) {
	global $post;
	
	$defaults = shortcode_atts( 
	  array(
	  'cols' => 8,
	  'rows' => 6), 
	  $atts 
	);
  
  $paged = ( get_query_var( 'paged' ) ) ? get_query_var('paged') : 1;
  $args = array(
    'post_type' => 'social_hashtag',
    'posts_per_page' => ($defaults['rows'] * $defaults['cols']),
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => $paged
  );
  $get_posts = new WP_Query($args);

?>  
    
<?php if( $get_posts->have_posts() ): ?>

  <div id='social_hashtags'>
  	<ul>
		<?php while( $get_posts->have_posts() ):  $get_posts->the_post(); ?>
			<?php	
	      $social_hashtag_userhandle = get_post_meta($post->ID, 'social_hashtag_userhandle', true);
	      $social_hashtag_thumb_url = get_post_meta($post->ID, 'social_hashtag_thumb_url', true);
	      $social_hashtag_full_url = get_post_meta($post->ID, 'social_hashtag_full_url', true);
	      $social_hashtag_platform = get_post_meta($post->ID, 'social_hashtag_platform', true);
	      $social_hashtag_thumb_imagesize = get_post_meta($post->ID, 'social_hashtag_thumb_imagesize', true);
			?>      
      <li class='<?php print $social_hashtag_platform  . " " . $social_hashtag_thumb_imagesize ?>'>
      	<a href='<?php print $social_hashtag_full_url ?>' target=_blank><img src='<?php print $social_hashtag_thumb_url ?>' title='<?php print $post->post_title ?>' alt='<?php print $post->post_title ?>' /></a>
      </li>
		<?php  endwhile; ?>
		</ul>
	</div>
 
<?php  endif; ?>

<?php } ?>