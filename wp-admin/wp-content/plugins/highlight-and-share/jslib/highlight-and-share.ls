jQuery !($) ->

  class HighlightAndShare
    (options) ->
      # extend default options
      @options = $.extend({},
        tooltipStyle: 'regular'
        tooltipColor: 'gray'
        iconsColor: 'white'
        via: 'test'
        opacity: 100
      , options)

      @url = encodeURIComponent(location.href)
      @title = $('title').text()
      @text = ''

      # define opacity styles
      op = @options.opacity
      if isNaN(parseInt(op))
        op = 100

      style = '-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=' + op + ')"; filter: alpha(opacity=' + op + '); -moz-opacity: ' + (op / 100) + '; -khtml-opacity: ' + (op / 100) + '; opacity: ' + (op / 100);

      # correct horizontal position of tooltip
      @sizes =
        'regular': -20
        'cloud': 30
        'shadowcloud': -45
        'gradientelegant': -5
        'gradient': -4

      # create share tooltip element
      @shareTooltip = $('<div style=\'' + style + '\' class="HighlightAndShare HighlightAndShare-icons-' + @options.iconsColor + ' HighlightAndShare-tooltip-' + @options.tooltipStyle + ' HighlightAndShare-tooltip-' + @options.tooltipStyle + '-' + @options.tooltipColor + '"></div>')
        .html('<div class="HighlightAndShare_FB"></div><div class="HighlightAndShare_TW"></div>')

      $('body').append(@shareTooltip)

      $('.HighlightAndShare_FB', @shareTooltip).on \click, @shareFacebook
      $('.HighlightAndShare_TW', @shareTooltip).on \click, @shareTwitter

      # when some text is selected globally on the page
      $ document .on \mouseup !(e) ~>
        window.setTimeout(~>
          # IE
          if document.selection
            selection = that

            if selection isnt 'Control'
              range = selection.createRange!
              selRect =
                left: range.boundingLeft
                top: range.boundingTop
                width: range.boundingWidth
                height: range.boundingHeight

              text = range.text

          # other browsers
          else if window.getSelection
            selection = window.getSelection!

            if selection.rangeCount and selection.anchorNode.nodeType is 3
              range = selection.getRangeAt(0)

              if range.getBoundingClientRect
                rect = range.getBoundingClientRect!
                selRect =
                  left: rect.left
                  top: rect.top
                  width: rect.right - rect.left
                  height: rect.bottom - rect.top

              text = selection.toString!

          # show share tooltip
          @showShareTooltip text, selRect
        , 100)


    # show sharing tooltip
    showShareTooltip: (text, selRect) ->
      if not text? or text.length == 0 or not selRect?
        @shareTooltip.hide()
      else
        @text = $.trim(text)
        @shareTooltip.css(
          top: (selRect.top + $(document).scrollTop! - @shareTooltip.outerHeight() - 8) + 'px'
          left: (selRect.left + ((selRect.width - @shareTooltip.outerWidth()) / 2) - @sizes[@options.tooltipStyle]) + 'px'
        ).fadeIn(200)

    # open popup window
    openWindow: (url, width, height) ->
      left = (screen.width - width) / 2
      top = (screen.height - height) / 2
      window.open(url, null, "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=#width,height=#height,top=#top,left=#left")

    # share with facebook
    shareFacebook: (e) ~>
      # window_url = "https://www.facebook.com/sharer/sharer.php?s=100&p%5Burl%5D=#{@url}&p%5Btitle%5D=#{@title}&p%5Bsummary%5D=#{@text}"
      url = location.href

      if url.indexOf('?') is -1 then url += '?' else url += '&'
      url += 'hastext=' + @text

      url = encodeURIComponent(url)

      window_url = "https://www.facebook.com/sharer/sharer.php?u=#{url}"
      @openWindow(window_url, 600, 400)

    # share with twitter
    shareTwitter: (e) ~>
      text = '"' + @text + '"'

      if text.length > 110 - @options.via.length
        text = text.substr(0, 107 - @options.via.length) + '..."'

      window_url = "https://twitter.com/share?url=#{@url}&text=#{text}&via=#{@options.via}&"
      @openWindow(window_url, 600, 400)


  new HighlightAndShare(HighlightAndShare_data.options);