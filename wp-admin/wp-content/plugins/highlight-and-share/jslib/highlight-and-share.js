jQuery(function($){
  var HighlightAndShare;
  HighlightAndShare = (function(){
    HighlightAndShare.displayName = 'HighlightAndShare';
    var prototype = HighlightAndShare.prototype, constructor = HighlightAndShare;
    function HighlightAndShare(options){
      var op, style, this$ = this;
      this.shareTwitter = bind$(this, 'shareTwitter', prototype);
      this.shareFacebook = bind$(this, 'shareFacebook', prototype);
      this.options = $.extend({}, {
        tooltipStyle: 'regular',
        tooltipColor: 'gray',
        iconsColor: 'white',
        via: 'test',
        opacity: 100
      }, options);
      this.url = encodeURIComponent(location.href);
      this.title = $('title').text();
      this.text = '';
      op = this.options.opacity;
      if (isNaN(parseInt(op))) {
        op = 100;
      }
      style = '-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=' + op + ')"; filter: alpha(opacity=' + op + '); -moz-opacity: ' + op / 100 + '; -khtml-opacity: ' + op / 100 + '; opacity: ' + op / 100;
      this.sizes = {
        'regular': -20,
        'cloud': 30,
        'shadowcloud': -45,
        'gradientelegant': -5,
        'gradient': -4
      };
      this.shareTooltip = $('<div style=\'' + style + '\' class="HighlightAndShare HighlightAndShare-icons-' + this.options.iconsColor + ' HighlightAndShare-tooltip-' + this.options.tooltipStyle + ' HighlightAndShare-tooltip-' + this.options.tooltipStyle + '-' + this.options.tooltipColor + '"></div>').html('<div class="HighlightAndShare_FB"></div><div class="HighlightAndShare_TW"></div>');
      $('body').append(this.shareTooltip);
      $('.HighlightAndShare_FB', this.shareTooltip).on('click', this.shareFacebook);
      $('.HighlightAndShare_TW', this.shareTooltip).on('click', this.shareTwitter);
      $(document).on('mouseup', function(e){
        window.setTimeout(function(){
          var that, selection, range, selRect, text, rect;
          if (that = document.selection) {
            selection = that;
            if (selection !== 'Control') {
              range = selection.createRange();
              selRect = {
                left: range.boundingLeft,
                top: range.boundingTop,
                width: range.boundingWidth,
                height: range.boundingHeight
              };
              text = range.text;
            }
          } else if (window.getSelection) {
            selection = window.getSelection();
            if (selection.rangeCount && selection.anchorNode.nodeType === 3) {
              range = selection.getRangeAt(0);
              if (range.getBoundingClientRect) {
                rect = range.getBoundingClientRect();
                selRect = {
                  left: rect.left,
                  top: rect.top,
                  width: rect.right - rect.left,
                  height: rect.bottom - rect.top
                };
              }
              text = selection.toString();
            }
          }
          return this$.showShareTooltip(text, selRect);
        }, 100);
      });
    }
    prototype.showShareTooltip = function(text, selRect){
      if (text == null || text.length === 0 || selRect == null) {
        return this.shareTooltip.hide();
      } else {
        this.text = $.trim(text);
        return this.shareTooltip.css({
          top: (selRect.top + $(document).scrollTop() - this.shareTooltip.outerHeight() - 8) + 'px',
          left: (selRect.left + (selRect.width - this.shareTooltip.outerWidth()) / 2 - this.sizes[this.options.tooltipStyle]) + 'px'
        }).fadeIn(200);
      }
    };
    prototype.openWindow = function(url, width, height){
      var left, top;
      left = (screen.width - width) / 2;
      top = (screen.height - height) / 2;
      return window.open(url, null, "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left);
    };
    prototype.shareFacebook = function(e){
      var url, window_url;
      url = location.href;
      if (url.indexOf('?') === -1) {
        url += '?';
      } else {
        url += '&';
      }
      url += 'hastext=' + this.text;
      url = encodeURIComponent(url);
      window_url = "https://www.facebook.com/sharer/sharer.php?u=" + url;
      return this.openWindow(window_url, 600, 400);
    };
    prototype.shareTwitter = function(e){
      var text, window_url;
      text = '"' + this.text + '"';
      if (text.length > 110 - this.options.via.length) {
        text = text.substr(0, 107 - this.options.via.length) + '..."';
      }
      window_url = "https://twitter.com/share?url=" + this.url + "&text=" + text + "&via=" + this.options.via + "&";
      return this.openWindow(window_url, 600, 400);
    };
    return HighlightAndShare;
  }());
  new HighlightAndShare(HighlightAndShare_data.options);
});
function bind$(obj, key, target){
  return function(){ return (target || obj)[key].apply(obj, arguments) };
}