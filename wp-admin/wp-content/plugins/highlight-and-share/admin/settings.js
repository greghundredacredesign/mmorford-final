jQuery(function($)
{
  var preview_tooltip = $('.HighlightAndShare'),
    preview_icons = preview_tooltip.find('div').first(),
    tooltipStyle = HighlightAndShare.options.tooltipStyle,
    $tooltip_color = $('#tooltip_color'),
    tooltipColor = HighlightAndShare.options.tooltipColor,
    tooltipStyles = HighlightAndShare.tooltipStyles;

  function updateTooltip()
  {
    preview_tooltip.attr('class', 'HighlightAndShare')
      .addClass('HighlightAndShare-tooltip-' + tooltipStyle)
      .addClass('HighlightAndShare-tooltip-' + tooltipStyle + '-' + tooltipColor);
  }

  $('#tooltip_style').on('change', function()
  {
    tooltipStyle = $(this).val();

    // recreate tooltip colors
    $tooltip_color.html('');

    for(var i in tooltipStyles[tooltipStyle].colors)
      $('<option></option>').val(i).text(tooltipStyles[tooltipStyle].colors[i]).appendTo($tooltip_color);

    tooltipColor = $tooltip_color.children('option:first').val();

    updateTooltip();
  });

  $tooltip_color.on('change', function()
  {
    tooltipColor = $(this).val();
    updateTooltip();
  });

  $('#icons_color').on('change', function()
  {
    preview_icons.attr('class', '').addClass('HighlightAndShare-icons-' + $(this).val());
  });

  $('#opacity').on('change keyup', function()
  {
    var v = $(this).val();
    preview_tooltip.css('opacity', !isNaN(parseInt(v))?v / 100:1);
  }).trigger('change');

});