<div class="wrap">
  <?php screen_icon('options-general'); ?>
  <h2><?php _e('Highlight and Share', self::ld); ?></h2>

  <?php
  if ($message == 'saved')
    echo '<div class="updated"><p>'.__('Settings were successfully saved.', self::ld).'</p></div>';
  ?>

  <form action="<?php echo $url; ?>" method="post">
    <?php wp_nonce_field(self::nonce); ?>

    <table class="form-table">
      <tr>
        <th scope="row"><label for="twitter_username"><?php _e('Twitter Username', self::ld); ?></label></th>
        <td>
          <input class="regular-text" type="text" name="twitter_username" id="twitter_username" value="<?php echo $this->getSetting('via'); ?>" />
        </td>
      </tr>

      <tr>
        <th scope="row"><label for="tooltip_style"><?php _e('Tooltip Style', self::ld); ?></label></th>
        <td>
          <select name="tooltip_style" id="tooltip_style">
            <?php
            $tooltipStyle = $this->getSetting('tooltipStyle');
            foreach($this->tooltip_styles as $style => $name)
              echo '<option value="'.$style.'"'.($style == $tooltipStyle?' selected':'').'>'.$name['name'].'</option>';
            ?>
          </select>
        </td>
      </tr>

      <tr>
        <th scope="row"><label for="tooltip_color"><?php _e('Tooltip Color', self::ld); ?></label></th>
        <td>
          <select name="tooltip_color" id="tooltip_color">
            <?php
            $tooltipColor = $this->getSetting('tooltipColor');
            foreach($this->tooltip_styles[$tooltipStyle]['colors'] as $color => $name)
              echo '<option value="'.$color.'"'.($tooltipColor == $color?' selected':'').'>'.$name.'</option>';
            ?>
          </select>
        </td>
      </tr>

      <tr>
        <th scope="row"><label for="icons_color"><?php _e('Icons Color', self::ld); ?></label></th>
        <td>
          <select name="icons_color" id="icons_color">
            <?php
            $iconsColor = $this->getSetting('iconsColor');
            foreach($this->icon_colors as $color => $name)
              echo '<option value="'.$color.'"'.($iconsColor == $color?' selected':'').'>'.$name.'</option>';
            ?>
          </select>
        </td>
      </tr>

      <tr>
        <th scope="row"><label for="opacity"><?php _e('Tooltip Opacity', self::ld); ?></label></th>
        <td>
          <input type="number" class="opacity-input" name="opacity" size="10" min="1" max="100" id="opacity" value="<?php echo $this->getSetting('opacity'); ?>" /> %
        </td>
      </tr>

      <tr>
        <th scope="row"><label for="preview"><?php _e('Preview', self::ld); ?></label></th>
        <td>
          <div class="HighlightAndShare HighlightAndShare-tooltip-<?php echo $tooltipStyle; ?> HighlightAndShare-tooltip-<?php echo $tooltipStyle; ?>-<?php echo $tooltipColor; ?>">
            <div class="HighlightAndShare-icons-<?php echo $iconsColor; ?>">
              <div class="HighlightAndShare_FB"></div><div class="HighlightAndShare_TW"></div>
            </div>
          </div>
        </td>
      </tr>

    </table>

    <?php submit_button(__('Save Changes', self::ld), 'primary', 'save_changes', true); ?>
  </form>
</div>