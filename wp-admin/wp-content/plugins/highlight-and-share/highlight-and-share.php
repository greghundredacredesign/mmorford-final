<?php
/*
Plugin Name: Highlight and Share
Plugin URI: http://www.creativindie.com/highlight-and-share-wordpress-plugin-for-facebook-and-twitter/
Description: It's tough to get savvy readers to share your content. Give your visitors some extra motivation with this stylish and simple plugin. Highlighting any text prompts a customizable pop-up to quickly share that text on Twitter or Facebook. The ability to select and share key passages that are interesting to specific readers and relevant to their personal networks will drastically improve social interaction and spread your content like wildfire.
Author: Derek Murphy (@Creativindie)
Version: 99999
Author URI: http://www.creativindie.com
License: GPLv2 or later
Text Domain: highlight-and-share
*/

// Was version 0.0.5

if (!defined('ABSPATH')) die();

class HighlightAndShare
{
  // version of the plugin - should be changed with the version in the header
  const version = '9999';

  // language domain
  const ld = 'highlight-and-share';
  const nonce = 'highlight-and-share-nonce';

  private $_url, $_path, $settings;

  protected $default_settings, $tooltip_styles, $tooltip_colors, $icon_colors;

  public function __construct()
  {
    // paths
    $this->_url = plugins_url('', __FILE__);
    $this->_path = dirname(__FILE__);

    // default settings
    $this->default_settings = array(
      'tooltipStyle' => 'regular',
      'tooltipColor' => 'gray',
      'iconsColor' => 'white',
      'via' => 'test',
      'opacity' => 100
    );

    // tooltip styles
    $this->tooltip_styles = array(
      'regular' => array(
        'name' => __('Regular', self::ld),
        'colors' => array(
          'gray' => __('Gray', self::ld),
          'blue' => __('Blue', self::ld),
          'green' => __('Green', self::ld),
          'orange' => __('Orange', self::ld),
          'purple' => __('Purple', self::ld),
          'red' => __('Red', self::ld),
          'white' => __('White', self::ld),
          'lightgray' => __('Lightgray', self::ld)
        )
      ),
      'cloud' => array(
        'name' => __('Cloud', self::ld),
        'colors' => array(
          'gray' => __('Gray', self::ld),
          'blue' => __('Blue', self::ld),
          'green' => __('Green', self::ld),
          'orange' => __('Orange', self::ld),
          'purple' => __('Purple', self::ld),
          'red' => __('Red', self::ld),
          'white' => __('White', self::ld),
          'lightgray' => __('Lightgray', self::ld)
        )
      ),
      'shadowcloud' => array(
        'name' => __('Shadow Cloud', self::ld),
        'colors' => array(
          'black' => __('Black', self::ld),
          'gray' => __('Gray', self::ld),
          'lightblue' => __('Light Blue', self::ld),
          'lime' => __('Lime', self::ld),
          'orange' => __('Orange', self::ld),
          'pink' => __('Pink', self::ld),
          'white' => __('White', self::ld)
        )
      ),
      'gradientelegant' => array(
        'name' => __('Elegant Gradient', self::ld),
        'colors' => array(
          'blue' => __('Blue', self::ld),
          'gray' => __('Gray', self::ld),
          'green' => __('Green', self::ld),
          'pink' => __('Pink', self::ld),
          'red' => __('Red', self::ld),
          'turquoise' => __('Turquoise', self::ld),
          'white' => __('White', self::ld)
        )
      ),
      'gradient' => array(
        'name' => __('Gradient', self::ld),
        'colors' => array(
          'orange' => __('Orange', self::ld),
          'red' => __('Red', self::ld),
          'white' => __('White', self::ld)
        )
      )
    );

    // icon colors
    $this->icon_colors = array(
      'white' => __('White', self::ld),
      'roundwhite' => __('Round White', self::ld),
      'black' => __('Black', self::ld),
      'roundblack' => __('Round Black', self::ld),
      'color' => __('Color', self::ld),
      'roundcolor' => __('Round Color', self::ld)
    );


    $this->settings = get_option(__class__.'_settings', $this->default_settings);

    add_action('plugins_loaded', array($this, 'plugins_loaded'));

    if (is_admin())
    {
      add_action('admin_init', array($this, 'admin_init'));
      add_action('admin_menu', array($this, 'admin_menu'));

      // add styles to the settings page
      add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
    }
    else
    {
      add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));

      if (isset($_GET['hastext']))
      	add_action('wp_head', array($this, 'wp_head'));
    }

    // on activation and uninstallation (this hook must bind to a static method)
    register_activation_hook(__FILE__, array($this, 'activation'));
    register_uninstall_hook(__FILE__, array(__class__, 'uninstall'));
  }

  // on activation
  public function activation()
  {
    add_option(__class__.'_settings', $this->default_settings);
  }

  // on uninstallation
  static function uninstall()
  {
    delete_option(__class__.'_settings');
  }

  // load language file
  public function plugins_loaded()
  {
    load_plugin_textdomain(self::ld, false, dirname(plugin_basename(__FILE__)).'/languages/');
  }

  // add submenu item to the settings menu
  public function admin_menu()
  {
    add_options_page(__('Highlight and Share', self::ld), __('Highlight and Share', self::ld), 'manage_options', __class__, array($this, 'settings_page'));
    add_filter('plugin_action_links_'.plugin_basename(__FILE__), array(&$this, 'filter_plugin_actions'), 10, 2);
  }

  // add a shortcut to the settings page on the plugins page
  public function filter_plugin_actions($l, $file)
  {
    $settings_link = '<a href="options-general.php?page='.__class__.'">'.__('Settings').'</a>';
    array_unshift($l, $settings_link);
    return $l;
  }

  // add scripts/styles to the settings page
  public function admin_enqueue_scripts($hook)
  {
    if ($hook == 'settings_page_HighlightAndShare')
    {
      wp_enqueue_style(__class__.'_has', $this->_url.'/jslib/highlight-and-share.css', array(), self::version, 'all');
      wp_enqueue_style(__class__, $this->_url.'/admin/settings.css', array(), self::version, 'all');
      wp_enqueue_script(__class__, $this->_url.'/admin/settings.js', array('jquery'), self::version, false);
      wp_localize_script(__class__, __class__, array(
        'tooltipStyles' => $this->tooltip_styles,
        'options' => array(
          'tooltipStyle' => $this->getSetting('tooltipStyle'),
          'tooltipColor' => $this->getSetting('tooltipColor'),
          'iconsColor' => $this->getSetting('iconsColor'),
          'via' => $this->getSetting('via')
        )
      ));
    }
  }

  // hook admin_init
  public function admin_init()
  {
    // action to save options
    if (isset($_POST['save_changes']) && $_POST['save_changes'])
    {
      if (!wp_verify_nonce($_POST['_wpnonce'], self::nonce))
        die(__('Whoops! There was a problem with the data you posted. Please go back and try again.', self::ld));

      $settings = array(
        'tooltipStyle' => isset($_POST['tooltip_style'])?$_POST['tooltip_style']:$this->default_settings['tooltipStyle'],
        'tooltipColor' => isset($_POST['tooltip_color'])?$_POST['tooltip_color']:$this->default_settings['tooltipColor'],
        'iconsColor' => isset($_POST['icons_color'])?$_POST['icons_color']:$this->default_settings['iconsColor'],
        'via' => isset($_POST['twitter_username'])?$_POST['twitter_username']:$this->default_settings['via'],
        'opacity' => isset($_POST['opacity'])?$_POST['opacity']:$this->default_settings['opacity']
      );

      update_option(__class__.'_settings', $settings);

      wp_redirect(admin_url('options-general.php?page='.__class__.'&message=saved'));
      exit;
    }
  }

  // settings page
  public function settings_page()
  {
    $url = admin_url('options-general.php?page='.__class__);
    $message = isset($_GET['message'])?$_GET['message']:false;

    require_once $this->_path.'/admin/settings.php';
  }

  // enqueue script in the frontend
  public function wp_enqueue_scripts()
  {
    wp_enqueue_style(__class__, $this->_url.'/jslib/highlight-and-share.css', array(), self::version, 'all');
    wp_enqueue_script(__class__, $this->_url.'/jslib/highlight-and-share.js', array('jquery'), self::version, false);
    wp_localize_script(__class__, __class__.'_data', array(
      'options' => array(
        'tooltipStyle' => $this->getSetting('tooltipStyle'),
        'tooltipColor' => $this->getSetting('tooltipColor'),
        'iconsColor' => $this->getSetting('iconsColor'),
        'via' => $this->getSetting('via'),
        'opacity' => $this->getSetting('opacity')
      )
    ));
  }

  public function wp_head()
  {
  	if (isset($_GET['hastext']) && $_GET['hastext'])
			echo '<meta property="og:description" content="'.esc_attr($_GET['hastext']).'" />';
  }

  // helper functions
  protected function getSetting($name)
  {
    if (isset($this->settings[$name]) && $this->settings[$name])
      return self::strip(stripslashes($this->settings[$name]));

    return $this->default_settings[$name];
  }

  static function strip($t)
  {
    return htmlentities($t, ENT_COMPAT, 'UTF-8');
  }
}

new HighlightAndShare();