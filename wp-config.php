<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mm-localhost');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'WP_DEBUG', TRUE );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*E!>d.>>!`1)l*P?Rr-XZe:C;BYIv2,)WB>WhQ>PYNL9;Kj&-H;:X,l[#Q)aH TI');
define('SECURE_AUTH_KEY',  '$oyp`5<,wY&)a>8[:x8}ao|O!OlredQ1KdRj=i^:TJ5E^7i:)tZ#V r4ARDPlErL');
define('LOGGED_IN_KEY',    'aTd]$c%d$t2=g<{KIC}_>Mp~#6cs8U*]i:osE$hCuUfp[)2%Ku2H>;eXI~S4[%iz');
define('NONCE_KEY',        'l!MZ(=Q0QKEBy2Tiw(0Vg($EEgr#)~Y+%$-/w!]v> 2!.&+9P;G$VX{pUu7=xRt?');
define('AUTH_SALT',        '8SfY71Wh1m#J}by5d(C-uK?:o<y`BR<RV<TsV#raD}M EG^9G,mq7/Ov%Om~VrBT');
define('SECURE_AUTH_SALT', '!c;8lKoPiK7/OW+jI2$k*/]qieq9C An=KO[~+%[pxT3Im{.Bye_mMj*N!-678v*');
define('LOGGED_IN_SALT',   'ymK:?SA<h[2+.`g+wkoa/r?MiPwxhdX~Terr!rj8ZG:Ot3j2Jpp@vUd14>i*Ryo*');
define('NONCE_SALT',       '?Yx<$|0p_{Ia(/,7OH4yCZq=lBU0Xqx?80cHgmON&CgPN(ZND/uL2M11C9;[&+&A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

