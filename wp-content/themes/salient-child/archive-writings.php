<?php get_header(); ?>

<div class="container-wrap">
		
	<div class="container main-content">
		
		<div class="row">
			
			<?php $options = get_nectar_theme_options(); 
			
			$blog_type = $options['blog_type'];
			if($blog_type == null) $blog_type = 'std-blog-sidebar';
			
			$masonry_class = null;
			$masonry_style = null;
			$infinite_scroll_class = null;
			$load_in_animation = (!empty($options['blog_loading_animation'])) ? $options['blog_loading_animation'] : 'none';
			$blog_standard_type = (!empty($options['blog_standard_type'])) ? $options['blog_standard_type'] : 'classic';

			//enqueue masonry script if selected
			if($blog_type == 'masonry-blog-sidebar' || $blog_type == 'masonry-blog-fullwidth' || $blog_type == 'masonry-blog-full-screen-width') {
				$masonry_class = 'masonry';
			}
			
			if($blog_type == 'masonry-blog-full-screen-width') {
				$masonry_class = 'masonry full-width-content';
			}
			
			if(!empty($options['blog_pagination_type']) && $options['blog_pagination_type'] == 'infinite_scroll'){
				$infinite_scroll_class = ' infinite_scroll';
			}

			if($masonry_class != null) {
				$masonry_style = (!empty($options['blog_masonry_type'])) ? $options['blog_masonry_type']: 'classic';
			}

			if($blog_standard_type == 'minimal' && $blog_type == 'std-blog-fullwidth')
				$std_minimal_class = 'standard-minimal full-width-content';
			else if($blog_standard_type == 'minimal' && $blog_type == 'std-blog-sidebar')
				$std_minimal_class = 'standard-minimal';
			else
				$std_minimal_class = '';
			
			if($blog_type == 'std-blog-sidebar' || $blog_type == 'masonry-blog-sidebar'){
				echo '<div id="post-area" class="col '.$std_minimal_class.' span_9 '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'"> <div class="posts-container"  data-load-animation="'.$load_in_animation.'">';
			} else {
				echo '<div id="post-area" class="col '.$std_minimal_class.' span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'"> <div class="posts-container"  data-load-animation="'.$load_in_animation.'">';
			}
			
			?>

			<div class="facetwp-template">
				<?php if( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title"><?php echo post_type_archive_title( '', false ); ?></h1>
					</header><!-- .page-header -->
				
					<?php
					while(have_posts()) : the_post();
		
					get_template_part( 'includes/post-templates/entry', 'writings-archive' );

				endwhile; endif; ?>
			</div>

		</div><!--/posts container-->
				
		<?php nectar_pagination(); ?>
			
		</div><!--/span_9-->
		
		<?php  if($blog_type == 'std-blog-sidebar' || $blog_type == 'masonry-blog-sidebar') { ?>
			<div id="sidebar" class="col span_3 col_last">
				<?php get_sidebar( 'writings' ); ?>
			</div><!--/span_3-->
		<?php } ?>
			
		</div><!--/row-->
		
	</div><!--/container-->

</div><!--/container-wrap-->
	
<?php get_footer(); ?>