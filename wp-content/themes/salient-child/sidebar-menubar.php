<div class="menubar-widgets">
<?php
if ( function_exists('dynamic_sidebar')) {
	if ( is_active_sidebar( 'sidebar-menubar' ) ) {
		dynamic_sidebar( 'sidebar-menubar' );
	}
}
?>
</div>
