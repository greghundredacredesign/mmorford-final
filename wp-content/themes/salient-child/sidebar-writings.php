<?php if ( function_exists('dynamic_sidebar')) {
	
	if ( is_active_sidebar( 'sidebar-writings' ) ) {
		dynamic_sidebar( 'sidebar-writings' );
	}
	else {
		dynamic_sidebar('Page Sidebar');
	}
	
} ?>
    
