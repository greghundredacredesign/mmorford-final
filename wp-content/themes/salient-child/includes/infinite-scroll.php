<?php
/**
 * Implement infinite scroll for single post pages
 */



/**
 * Enqueue scripts and styles for the infinite scroll feature.
 */
function salient_child_infinite_scroll_enqueue_scripts() {
  wp_enqueue_script ( 'inview', get_stylesheet_directory_uri() . '/js/in-view.min.js' );
  wp_enqueue_script ( 'infinite-scroll', get_stylesheet_directory_uri() . '/js/infinite-scroll.js', [ 'jquery' ] );
  wp_localize_script( 'infinite-scroll', 'AjaxParams', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'salient_child_infinite_scroll_enqueue_scripts' );



/**
 * Append next post hidden field to post content
 * to allow identify which post to load while scroll.
 */
function salient_child_append_next_post_field() {
  global $post;

  // Get next post id for infinite scroll
  $next_post = get_previous_post();
  $next_post_id = $next_post ? $next_post->ID : -1;

  echo '<input type="hidden" class="next-post-id" value="' . esc_attr( $next_post_id ) . '">';
}
add_action( 'salient_child_before_post', 'salient_child_append_next_post_field' );



/**
 * Get post content and return it.
 * @return JSON Post content or error data for ajax call.
 */
function salient_child_get_next_post_callback() {

  // Start buffering
  ob_start();

  $post_id = $_POST['post_id'];

  // Get post
  global $post;
  $post = get_post( $post_id );

  if ( $post ) {
    setup_postdata( $post );

    // Get post content template
    get_template_part( 'includes/post-templates/entry', 'single-ajax' );

    // Get content from buffer
    $content = ob_get_clean();

    // Send post content
    wp_send_json( array(
        'post_id'       => $post_id,
        'post_content'  => $content
      ) );
  }
  else {
    // Send error message
    wp_send_json( array(
        'end_of_posts'  => true,
        'message'       => __( 'End of posts.', 'salient_child' )
      ) );
  }

  // Finish request
  wp_die();
}
add_action('wp_ajax_salient_child_get_next_post', 'salient_child_get_next_post_callback' );
add_action('wp_ajax_nopriv_salient_child_get_next_post', 'salient_child_get_next_post_callback' );