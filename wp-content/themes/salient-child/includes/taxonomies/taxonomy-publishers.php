<?php
/**
 * Register custom taxonomy 'Publishers'.
 */

// REGISTER TAXONOMY
function salient_child_custom_taxonomy_publishers() {

  $labels = array(
    'name'                       => _x( 'Publishers', 'taxonomy general name', 'salient_child' ),
    'singular_name'              => _x( 'Publisher', 'taxonomy singular name', 'salient_child' ),
    'search_items'               => __( 'Search Publishers', 'salient_child' ),
    'popular_items'              => __( 'Common Publishers', 'salient_child' ),
    'all_items'                  => __( 'All Publishers', 'salient_child' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Publisher', 'salient_child' ),
    'update_item'                => __( 'Update Publisher', 'salient_child' ),
    'add_new_item'               => __( 'Add New Publisher', 'salient_child' ),
    'new_item_name'              => __( 'New Publisher Name', 'salient_child' ),
    'separate_items_with_commas' => __( 'Separate Publishers with commas', 'salient_child' ),
    'add_or_remove_items'        => __( 'Add or remove Publisher', 'salient_child' ),
    'choose_from_most_used'      => __( 'Choose from the most common Publisher', 'salient_child' ),
    'not_found'                  => __( 'No Publishers found.', 'salient_child' ),
    'menu_name'                  => __( 'Publishers', 'salient_child' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'publisher' ),
  );

  register_taxonomy( 'publishers', array( 'writings' ), $args );
}
add_action( 'init', 'salient_child_custom_taxonomy_publishers', 0 );
