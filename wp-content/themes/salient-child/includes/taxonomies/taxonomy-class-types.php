<?php
/**
 * Register custom taxonomy 'Class Types'.
 */

// REGISTER TAXONOMY
function salient_child_custom_taxonomy_class_types() {

  $labels = array(
    'name'                       => _x( 'Class Types', 'taxonomy general name', 'salient_child' ),
    'singular_name'              => _x( 'Class Type', 'taxonomy singular name', 'salient_child' ),
    'search_items'               => __( 'Search Class Types', 'salient_child' ),
    'popular_items'              => __( 'Common Class Types', 'salient_child' ),
    'all_items'                  => __( 'All Class Types', 'salient_child' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Class Type', 'salient_child' ),
    'update_item'                => __( 'Update Class Type', 'salient_child' ),
    'add_new_item'               => __( 'Add New Class Type', 'salient_child' ),
    'new_item_name'              => __( 'New Class Type Name', 'salient_child' ),
    'separate_items_with_commas' => __( 'Separate Class Types with commas', 'salient_child' ),
    'add_or_remove_items'        => __( 'Add or remove Class Type', 'salient_child' ),
    'choose_from_most_used'      => __( 'Choose from the most common Class Type', 'salient_child' ),
    'not_found'                  => __( 'No Class Types found.', 'salient_child' ),
    'menu_name'                  => __( 'Class Types', 'salient_child' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'class_type' ),
  );

  register_taxonomy( 'class_types', array( 'yoga_workshops' ), $args );
}
add_action( 'init', 'salient_child_custom_taxonomy_class_types', 0 );
