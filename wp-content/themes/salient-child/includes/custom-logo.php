<?php
/**
 * Provide logo image upload fields on the Customizer
 */



function salient_child_customize_register_logo( $wp_customize ) {
  // LOGO DESKTOP
  $wp_customize->add_setting( 'salient_child_logo_desktop' );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'salient_child_logo_desktop', array(
    'label'    => __( 'Logo Desktop', 'salient_child' ),
    'section'  => 'title_tagline',
    'settings' => 'salient_child_logo_desktop',
  ) ) );

  // LOGO MOBILE
  $wp_customize->add_setting( 'salient_child_logo_mobile' );
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'salient_child_logo_mobile', array(
    'label'    => __( 'Logo Mobile', 'salient_child' ),
    'section'  => 'title_tagline',
    'settings' => 'salient_child_logo_mobile',
  ) ) );
}
add_action( 'customize_register', 'salient_child_customize_register_logo' );




/**
 * Customize logo output to use mobile and desktop versions
 */
if (!function_exists('nectar_logo_output')) {
  function nectar_logo_output($activate_transparency = false, $off_canvas_style = 'slide-out-from-right') {

    // Get logo options
    $logo_desktop = get_theme_mod( 'salient_child_logo_desktop' );
    $logo_mobile = get_theme_mod( 'salient_child_logo_mobile', $logo_desktop ); // Defaults to same as desktop

    if ( $logo_desktop ) {
      ?>
      <style type="text/css">
        header#top #logo img.logo--mobile,
        header#top #logo img.logo--desktop { display: none; }

        @media (max-width: 1000px) {
          header#top #logo img.logo--mobile {
            height: 24px !important;
            display: unset;
          }
        }

        @media (min-width: 1001px) {
          header#top #logo img.logo--desktop {
            height: 100px !important;
            display: unset;
          }
        }
      </style>

      <img class="logo--mobile" src='<?php echo esc_url( $logo_mobile ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
      <img class="logo--desktop" src='<?php echo esc_url( $logo_desktop ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
      <?php
    }
  }
}
