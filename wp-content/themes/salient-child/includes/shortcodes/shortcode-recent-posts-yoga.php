<?php
/**
 * Implements recent_posts_yoga shortcode.
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



class SalientChildRecentPostsYogaShortcode {
  
  protected static $_instance = null;

  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }



  public function __construct() {
    add_action( 'init', array( $this, 'register_shortcode' ), 0 );
  }





  public function register_shortcode() {
    add_shortcode('recent_posts_yoga', array( $this, 'render_shortcode' ) );
  }



  public function get_post_class_type_meta( $post_id ) {
    $return_string = '';

    $class_types = get_the_terms( $post_id, 'class_types' );

    foreach ( $class_types as $key => $class_type ) {
      if ( $key > 0 ) { $return_string .= ' | '; }
      $return_string .= '<span class="meta-class-type">' . $class_type->name . '</span>';
    }

    return $return_string;
  }




  /**
   * Render the shortcode front-end html
   *
   * NOTE: this code is copied from nectar recent posts shortcode
   * and addapted to work with a different post type.
   *
   * Original code can be found at
   * wp-content/themes/salient/nectar/tinymce/shortcode-processing.php ln 2991
   */
  public function render_shortcode( $atts ) {
    extract(
      shortcode_atts(
        array(
          'category' => 'all',
          'since' => '',
          'slider_size' => '600',
          'color_scheme' => 'light',
          'slider_above_text' => '',
          'posts_per_page' => '4',
          'columns' => '4',
          'style' => 'default',
          'post_offset' => '0'
        ), $atts ) );

    $post_type = 'yoga_workshops';

    global $post;  
    global $options;
    
    $posts_page_id = get_option('page_for_posts');
    $posts_page = get_page($posts_page_id);
    $posts_page_title = $posts_page->post_title;
    $posts_page_link = get_page_uri($posts_page_id);
    
    // Get query string filter: category
    if ( !empty( $_GET['category'] ) ) {
      $category = sanitize_text_field( $_GET['category'] );
    }

    // Get query string filter: time since
    if ( !empty( $_GET['since'] ) ) {
      $since = sanitize_text_field( $_GET['since'] );
    }

    //incase only all was selected
    if($category == 'all') {
      $category = null;
    }
    
    if($style != 'slider') {
        
        ob_start();
        ?>
        
        <div class="row blog-recent columns-<?php echo $columns; ?>" data-style="<?php echo $style; ?>" data-color-scheme="<?php echo $color_scheme; ?>">
          
          <?php 

            $tax_query = array(
              array( 'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array('post-format-link','post-format-quote'),
                'operator' => 'NOT IN'
                )
            );

            if ( $category ) {
              $tax_query[] = array( 'taxonomy' => 'class_types',
                'field' => 'slug',
                'terms' => explode( ',', $category ),
                'operator' => 'IN'
                );
            }

            $recentBlogPosts = array(
              'showposts' => $posts_per_page,
              'ignore_sticky_posts' => 1,
              'post_type' => $post_type,
              'offset' => $post_offset,
              'tax_query' => $tax_query,
            );

            if ( !empty( $since ) ) {
              $recentBlogPosts['date_query'] = array(
                'column'  => 'post_date',
                'after'   => '- ' . $since
              );
            }

          $recent_posts_query = new WP_Query($recentBlogPosts);  

          if( $recent_posts_query->have_posts() ) :  while( $recent_posts_query->have_posts() ) : $recent_posts_query->the_post();  


          if($columns == '4') {
            $col_num = 'span_3';
          } else if($columns == '3') {
            $col_num = 'span_4';
          } else if($columns == '2') {
            $col_num = 'span_6';
          } else {
            $col_num = 'span_12';
          }
          
          ?>

          <div class="col <?php echo $col_num; ?>">
            
            <?php 
              
              $wp_version = floatval(get_bloginfo('version'));
              
              if($style == 'default') {

                if(get_post_format() == 'video'){

                   if ( $wp_version < "3.6" ) {
                     $video_embed = get_post_meta($post->ID, '_nectar_video_embed', true);
                      
                           if( !empty( $video_embed ) ) {
                               echo '<div class="video-wrap">' . stripslashes(htmlspecialchars_decode($video_embed)) . '</div>';
                           } else { 
                               //nectar_video($post->ID); 
                           }
                   }
                     else {
                    
                    $video_embed = get_post_meta($post->ID, '_nectar_video_embed', true);
                      $video_m4v = get_post_meta($post->ID, '_nectar_video_m4v', true);
                      $video_ogv = get_post_meta($post->ID, '_nectar_video_ogv', true); 
                      $video_poster = get_post_meta($post->ID, '_nectar_video_poster', true); 
                    
                      if( !empty($video_embed) || !empty($video_m4v) ){
          
                             $wp_version = floatval(get_bloginfo('version'));
                          
                      //video embed
                      if( !empty( $video_embed ) ) {
                      
                               echo '<div class="video">' . do_shortcode($video_embed) . '</div>';
                      
                          } 
                          //self hosted video pre 3-6
                          else if( !empty($video_m4v) && $wp_version < "3.6") {
                          
                               echo '<div class="video">'; 
                                   //nectar_video($post->ID); 
                         echo '</div>'; 
                       
                          } 
                          //self hosted video post 3-6
                          else if($wp_version >= "3.6"){
              
                            if(!empty($video_m4v) || !empty($video_ogv)) {
                            
                          $video_output = '[video ';
                        
                          if(!empty($video_m4v)) { $video_output .= 'mp4="'. $video_m4v .'" '; }
                          if(!empty($video_ogv)) { $video_output .= 'ogv="'. $video_ogv .'"'; }
                        
                          $video_output .= ' poster="'.$video_poster.'"]';
                        
                              echo '<div class="video">' . do_shortcode($video_output) . '</div>';  
                            }
                          }
                    
                     } // endif for if there's a video
                    
                    } // endif for 3.6 
                    
                } //endif for post format video
                
                else if(get_post_format() == 'audio'){ ?>
                  <div class="audio-wrap">    
                    <?php 
                    if ( $wp_version < "3.6" ) {
                        //nectar_audio($post->ID);
                    } 
                    else {
                      $audio_mp3 = get_post_meta($post->ID, '_nectar_audio_mp3', true);
                        $audio_ogg = get_post_meta($post->ID, '_nectar_audio_ogg', true); 
                      
                      if(!empty($audio_ogg) || !empty($audio_mp3)) {
                            
                        $audio_output = '[audio ';
                        
                        if(!empty($audio_mp3)) { $audio_output .= 'mp3="'. $audio_mp3 .'" '; }
                        if(!empty($audio_ogg)) { $audio_output .= 'ogg="'. $audio_ogg .'"'; }
                        
                        $audio_output .= ']';
                        
                            echo  do_shortcode($audio_output);  
                          }
                    } ?>
                  </div><!--/audio-wrap-->
                <?php }
                
                else if(get_post_format() == 'gallery'){
                  
                  if ( $wp_version < "3.6" ) {
                    
                    
                    if ( has_post_thumbnail() ) { echo get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')); }
                    
                  }
                  
                  else {
                    
                    $gallery_ids = grab_ids_from_gallery(); ?>
              
                    <div class="flex-gallery"> 
                         <ul class="slides">
                          <?php 
                          foreach( $gallery_ids as $image_id ) {
                               echo '<li>' . wp_get_attachment_image($image_id, 'portfolio-thumb', false) . '</li>';
                          } ?>
                          </ul>
                       </div><!--/gallery-->

                 <?php }
                      
                }
                
                else {
                  if ( has_post_thumbnail() ) { echo '<a href="' . get_permalink() . '">' . get_the_post_thumbnail($post->ID, 'portfolio-thumb', array('title' => '')) . '</a>'; }
                }
            
              ?>

                <div class="post-header">

                    <div class="class-type-yoga"><?php echo SalientChildRecentPostsYogaShortcode()->get_post_class_type_meta( $post->ID ); ?></div>

                  <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>  


                     <div class="yoga-read-more"><a href="<?php the_permalink(); ?>">Read More</a></div>






                </div>

                <!--/post-header-->
                
                <?php 


              } // default style


              else if($style == 'minimal') { ?>

                <a href="<?php the_permalink(); ?>"></a>
                <div class="post-header">
                  <span class="meta"> <?php echo get_the_date() . ' ' . __('in',NECTAR_THEME_NAME); ?> <?php the_category(', '); ?> </span> 
                  <h3 class="title"><?php the_title(); ?></h3>  
                </div><!--/post-header-->
                <?php the_excerpt(); ?>
                <span><?php echo __('Read More',NECTAR_THEME_NAME); ?> <i class="icon-button-arrow"></i></span>

              <?php } else if($style == 'title_only') { ?>

                <a href="<?php the_permalink(); ?>"></a>
                <div class="post-header">
                  <span class="meta"> <?php echo get_the_date(); ?> </span> 
                  <h2 class="title"><?php the_title(); ?></h2>  
                </div><!--/post-header-->

              <?php } 

              else if($style == 'classic_enhanced' || $style == 'classic_enhanced_alt') { 

                if($columns == '4') {
                  $image_attrs =  array('title' => '', 'sizes' => '(min-width: 1300px) 25vw, (min-width: 1000px) 33vw, (min-width: 690px) 100vw, 100vw');
                } else if($columns == '3') {
                  $image_attrs =  array('title' => '', 'sizes' => '(min-width: 1300px) 33vw, (min-width: 1000px) 33vw, (min-width: 690px) 100vw, 100vw');
                } else if($columns == '2') {
                  $image_attrs =  array('title' => '', 'sizes' => '(min-width: 1600px) 50vw, (min-width: 1300px) 50vw, (min-width: 1000px) 50vw, (min-width: 690px) 100vw, 100vw');
                } else {
                  $image_attrs =  array('title' => '', 'sizes' => '(min-width: 1000px) 100vw, (min-width: 690px) 100vw, 100vw');
                } ?>

                <div <?php post_class('inner-wrap'); ?>>

                <?php
                if ( has_post_thumbnail() ) { 
                  if($style == 'classic_enhanced') {
                    echo'<a href="' . get_permalink() . '" class="img-link"><span class="post-featured-img">'.get_the_post_thumbnail($post->ID, 'portfolio-thumb', $image_attrs) .'</span></a>'; 
                  } else if($style == 'classic_enhanced_alt') {
                    $masonry_sizing_type = (!empty($options['portfolio_masonry_grid_sizing']) && $options['portfolio_masonry_grid_sizing'] == 'photography') ? 'photography' : 'default';
                    $cea_size = ($masonry_sizing_type == 'photography') ? 'regular_photography' : 'tall';
                    echo'<a href="' . get_permalink() . '" class="img-link"><span class="post-featured-img">'.get_the_post_thumbnail($post->ID, $cea_size, $image_attrs) .'</span></a>'; 
                  }
                } ?>

                <?php
                echo '<span class="meta-category">';
                $categories = get_the_category();
                if ( ! empty( $categories ) ) {
                  $output = null;
                    foreach( $categories as $category ) {
                        $output .= '<a class="'.$category->slug.'" href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>';
                    }
                    echo trim( $output);
                  }
                echo '</span>'; ?>
                  
                <a class="entire-meta-link" href="<?php the_permalink(); ?>"></a>

                <div class="article-content-wrap">
                  <div class="post-header">
                    <span class="meta"> <?php echo get_the_date(); ?> </span> 
                    <h3 class="title"><?php the_title(); ?></h3>  
                  </div><!--/post-header-->
                  <div class="excerpt">
                    <?php the_excerpt(); ?>
                  </div>
                </div>
                
                <div class="post-meta">
                  <span class="meta-author"> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"> <i class="icon-default-style icon-salient-m-user"></i> <?php the_author(); ?></a> </span> 
                  
                  <?php if(comments_open()) { ?>
                    <span class="meta-comment-count">  <a href="<?php comments_link(); ?>">
                      <i class="icon-default-style steadysets-icon-chat-3"></i> <?php comments_number( '0', '1','%' ); ?></a>
                    </span>
                  <?php } ?>
                  
                  <div class="nectar-love-wrap">
                    <?php if( function_exists('nectar_love') ) nectar_love(); ?>
                  </div><!--/nectar-love-wrap-->  
                </div>

              </div>

              <?php }  ?>
            
          </div><!--/col-->
          
          <?php endwhile; endif; 
              wp_reset_postdata();
          ?>
      
        </div><!--/blog-recent-->
      
      <?php

      wp_reset_query();
      
      $recent_posts_content = ob_get_contents();
      
      ob_end_clean();
    
    } // regular recent posts


    else { //slider


      ob_start(); 
        
      echo $title_label_output; ?>
      
      <?php

        $tax_query = array(
          array( 'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array('post-format-link','post-format-quote'),
            'operator' => 'NOT IN'
            )
        );

        if ( $category ) {
          $tax_query[] = array( 'taxonomy' => 'class_types',
            'field' => 'slug',
            'terms' => explode( ',', $category ),
            'operator' => 'IN'
            );
        }

        $recentBlogPosts = array(
          'showposts' => $posts_per_page,
          'ignore_sticky_posts' => 1,
          'post_type' => $post_type,
          'offset' => $post_offset,
          'tax_query' => $tax_query
        );

        if ( !empty( $since ) ) {
          $recentBlogPosts['date_query'] = array(
            'column'  => 'post_date',
            'after'   => '- ' . $since
          );
        }

      $recent_posts_query = new WP_Query($recentBlogPosts);  


        $animate_in_effect = (!empty($options['header-animate-in-effect'])) ? $options['header-animate-in-effect'] : 'none';
      echo '<div class="nectar-recent-posts-slider" data-height="'.$slider_size.'" data-animate-in-effect="'.$animate_in_effect.'">';

      echo '<div class="nectar-recent-posts-slider-inner">'; 
      $i = 0;
      if( $recent_posts_query->have_posts() ) :  while( $recent_posts_query->have_posts() ) : $recent_posts_query->the_post(); global $post; ?>

          <?php 
            $bg = get_post_meta($post->ID, '_nectar_header_bg', true);
            $bg_color = get_post_meta($post->ID, '_nectar_header_bg_color', true);
            $bg_image_id = null;
            $featured_img = null;
            
            if(!empty($bg)){
              //page header
              $featured_img = $bg;

            } elseif(has_post_thumbnail($post->ID)) {
              $bg_image_id = get_post_thumbnail_id($post->ID);
              $image_src = wp_get_attachment_image_src($bg_image_id, 'full');
              $featured_img = $image_src[0];
            }


          ?>

          <div class="nectar-recent-post-slide <?php if($bg_image_id == null) echo 'no-bg-img'; ?> post-ref-<?php echo $i; ?>">

            <div class="nectar-recent-post-bg"  style=" <?php if(!empty($bg_color)) { ?> background-color: <?php echo $bg_color;?>; <?php } ?> background-image: url(<?php echo $featured_img;?>);" > </div>

            <?php 

            echo '<div class="recent-post-container container"><div class="inner-wrap">';

            echo '<span class="strong">';
                $categories = get_the_category();
                if ( ! empty( $categories ) ) {
                  $output = null;
                    foreach( $categories as $category ) {
                        $output .= '<a class="'.$category->slug.'" href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', NECTAR_THEME_NAME), $category->name ) ) . '"><span class="'.$category->slug.'">'.esc_html( $category->name ) .'</span></a>';
                    }
                    echo trim( $output);
                }
              echo '</span>'; ?>
            
              <h2 class="post-ref-<?php echo $i; ?>"><a href=" <?php echo get_permalink(); ?>" class="full-slide-link"> <?php echo the_title(); ?> </a></h2> 
            </div></div>
              

          </div>

          <?php $i++; ?>

      <?php endwhile; endif; 

          wp_reset_postdata();
    
       echo '</div></div>';

      wp_reset_query();
      
      $recent_posts_content = ob_get_contents();
      
      ob_end_clean();
    }


    return $recent_posts_content;
  }
}



function SalientChildRecentPostsYogaShortcode() {
  return SalientChildRecentPostsYogaShortcode::instance();
}



// Get instance to initialize class instance
SalientChildRecentPostsYogaShortcode();
