<?php
/**
 * Add Widget Areas
 */



/**
 * Add custom widget areas
 */
function salient_child_widgets_init() {
  // WRITINGS
  register_sidebar( array(
    'name'          => __( 'Writings Sidebar', 'salient_child' ),
    'id'            => 'sidebar-writings',
    'description'   => __( 'Widgets added here are shown in the writings pages.', 'salient_child' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );

  // YOGA
  register_sidebar( array(
    'name'          => __( 'Yoga Sidebar', 'salient_child' ),
    'id'            => 'sidebar-yoga',
    'description'   => __( 'Widgets added here are shown in the yoga pages.', 'salient_child' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );

  // MENU BAR
  register_sidebar( array(
    'name'          => __( 'Menu Bar Widgets', 'salient_child' ),
    'id'            => 'sidebar-menubar',
    'description'   => __( 'Widgets added here are shown in the menu bar.', 'salient_child' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'salient_child_widgets_init' );

