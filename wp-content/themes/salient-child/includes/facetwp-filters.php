<?php
/**
 * Include assets necessary to FacetWP to work correcly with the theme
 */



/**
 * Enqueue scripts and styles for the infinite scroll feature.
 */
function salient_child_facetwp_enqueue_script() {
  wp_enqueue_script ( 'facetwp-init', get_stylesheet_directory_uri() . '/js/facetwp-init.js' );
}
add_action( 'wp_enqueue_scripts', 'salient_child_facetwp_enqueue_script' );
