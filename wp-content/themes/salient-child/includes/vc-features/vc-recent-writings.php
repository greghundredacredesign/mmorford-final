<?php
/**
 * VC Component: Recent Writings
 */

/**
 * Check if required classes exists before
 * All code in this file should be enclosed in this div
 */
if ( class_exists( 'WPBakeryShortCode' ) ) {

  /**
   * Register the component on Visual Composer Init.
   */
  function salient_child_register_vc_component_recent_writings() { 
    if ( class_exists( 'vcCustomRecentPostsWritings' ) ) {
      // Element Class Init
      new vcCustomRecentPostsWritings();
    }
  }
  add_action( 'vc_before_init', 'salient_child_register_vc_component_recent_writings' );


  // Element Class 
  class vcCustomRecentPostsWritings extends WPBakeryShortCode {

    public $post_type;
    public $post_type_name;
    public $display_name;
    public $shortcode_slug;

    // Element Init
    function __construct() {
      $this->post_type = 'writings';
      $this->display_name = __( 'Recent Writings', 'salient_child' );
      $this->post_type_name = __( 'Writings', 'salient_child' );
      $this->shortcode_slug = 'recent_posts_writings';

      add_action( 'init', array( $this, 'mapping' ) );
    }

    // Element Mapping
    public function mapping() {
      // Bail if VC is not enabled
      if ( !defined( 'WPB_VC_VERSION' ) ) {
        return;
      }

      // Get category options
      $is_admin = is_admin();
      $categories = ( $is_admin ) ? get_categories() : array( 'All' => 'all' );
      $category_options = array( 'All' => 'all' );
      if ( $is_admin ) {
        foreach ( $categories as $term ) {
          if ( isset( $term->name ) && isset( $term->slug ) )
            $category_options[ htmlspecialchars($term->name) ] = htmlspecialchars( $term->slug );
        }
      } else {
        $category_options[ 'All' ] = 'all';
      }
           
      // Map the block with vc_map()
      vc_map(
        array(
          'name' => $this->display_name,
          'base' => $this->shortcode_slug,
          'description' => sprintf( __('Recent posts of %s', 'salient_child'), $this->post_type_name ),
          'icon' => 'icon-wpb-recent-posts',
          'params' => array(
            array(
              'type' => 'dropdown',
              'heading' => __('Style', 'js_composer'),
              'param_name' => 'style',
              'admin_label' => true,
              'value' => array( 
                'Default' => 'default',
                'Minimal' => 'minimal',
                'Minimal - Title Only' => 'title_only',
                'Minimal - Side Thumbnail' => 'side-thumbnail',
                'Classic Enhanced' => 'classic_enhanced',
                'Classic Enhanced Alt' => 'classic_enhanced_alt',
                'Slider' => 'slider'
              ),
              'save_always' => true,
              'description' => __('Please select desired style here.', 'js_composer')
            ),
            array(
              'type' => 'dropdown',
              'heading' => __('Color Scheme', 'js_composer'),
              'param_name' => 'color_scheme',
              'admin_label' => true,
              'value' => array( 
                'Light' => 'light',
                'Dark' => 'dark',
              ),
              'dependency' => Array('element' => 'style', 'value' => array('classic_enhanced')),
              'save_always' => true,
              'description' => __('Please select your desired coloring here.', 'js_composer')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Slider Height', 'js_composer'),
                'param_name' => 'slider_size',
                'admin_label' => false,
                'dependency' => Array('element' => 'style', 'value' => 'slider'),
                'description' => __('Don\'t include "px" in your string. e.g. 650', 'js_composer')
              ),
            array(
              'type' => 'dropdown_multi',
              'heading' => __('Categories', 'js_composer'),
              'param_name' => 'category',
              'admin_label' => true,
              'value' => $category_options,
              'save_always' => true,
              'description' => __('Please select the categories you would like to display in your recent posts. <br/> You can select multiple categories too (ctrl + click on PC and command + click on Mac).', 'js_composer')
            ),
            array(
              'type' => 'dropdown',
              'heading' => __('Number Of Columns', 'js_composer'),
              'param_name' => 'columns',
              'admin_label' => false,
              'value' => array(
                '4' => '4',
                '3' => '3',
                '2' => '2',
                '1' => '1'
              ),
              'dependency' => Array('element' => 'style', 'value' => array('default','minimal','title_only','classic_enhanced', 'classic_enhanced_alt')),
              'save_always' => true,
              'description' => __('Please select the number of posts you would like to display.', 'js_composer')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Time since', 'js_composer'),
                'param_name' => 'since',
                'description' => __('Filter posts by time elapsed since published.<br/> Enter a human readable time such as `7 days`, `3 months`, `1 year`.', 'js_composer')
              ),
            array(
                'type' => 'textfield',
                'heading' => __('Number Of Posts', 'js_composer'),
                'param_name' => 'posts_per_page',
                'description' => __('How many posts would you like to display? <br/> Enter as a number example "4"', 'js_composer')
              ),
              array(
                'type' => 'textfield',
                'heading' => __('Post Offset', 'js_composer'),
                'param_name' => 'post_offset',
                'description' => __('Optioinally enter a number e.g. "2" to offset your posts by - useful for when you\'re using multiple styles of this element on the same page and would like them to no show duplicate posts', 'js_composer')
              )
          )
        )
      );  
    }

  } // End Element Class

} // End check if required classes exists
