<?php
/**
 * Register writings post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



function salient_child_custom_post_type_writings() {
  
  $labels = array(
    'name'               => _x( 'Writings', 'post type general name', 'salient_child' ),
    'singular_name'      => _x( 'Writing', 'post type singular name', 'salient_child' ),
    'menu_name'          => _x( 'Writings', 'admin menu', 'salient_child' ),
    'name_admin_bar'     => _x( 'Writing', 'add new on admin bar', 'salient_child' ),
    'add_new'            => _x( 'Add New', 'Writings', 'salient_child' ),
    'add_new_item'       => __( 'Add New Writing', 'salient_child' ),
    'new_item'           => __( 'New Writing', 'salient_child' ),
    'edit_item'          => __( 'Edit Writing', 'salient_child' ),
    'view_item'          => __( 'View Writing', 'salient_child' ),
    'all_items'          => __( 'All Writings', 'salient_child' ),
    'search_items'       => __( 'Search Writings', 'salient_child' ),
    'parent_item_colon'  => __( 'Parent Writings:', 'salient_child' ),
    'not_found'          => __( 'No Writings found.', 'salient_child' ),
    'not_found_in_trash' => __( 'No Writings found in Trash.', 'salient_child' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'menu_icon'          => 'dashicons-format-aside',
    'menu_position'      => 9,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'writings' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions' ),
    'taxonomies'         => array( 'category' ),
  );

  register_post_type( 'writings', $args );
}
add_action( 'init', 'salient_child_custom_post_type_writings', 0 );
