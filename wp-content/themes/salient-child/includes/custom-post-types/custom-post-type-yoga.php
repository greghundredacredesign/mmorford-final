<?php
/**
 * Register yoga post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



function salient_child_custom_post_type_yoga() {
  
  $labels = array(
    'name'               => _x( 'Yoga Items', 'post type general name', 'salient_child' ),
    'singular_name'      => _x( 'Yoga Item', 'post type singular name', 'salient_child' ),
    'menu_name'          => _x( 'Yoga', 'admin menu', 'salient_child' ),
    'name_admin_bar'     => _x( 'Yoga Item', 'add new on admin bar', 'salient_child' ),
    'add_new'            => _x( 'Add New', 'Yoga Items', 'salient_child' ),
    'add_new_item'       => __( 'Add New Yoga Item', 'salient_child' ),
    'new_item'           => __( 'New Yoga Item', 'salient_child' ),
    'edit_item'          => __( 'Edit Yoga Item', 'salient_child' ),
    'view_item'          => __( 'View Yoga Item', 'salient_child' ),
    'all_items'          => __( 'All Yoga Items', 'salient_child' ),
    'search_items'       => __( 'Search Yoga Items', 'salient_child' ),
    'parent_item_colon'  => __( 'Parent Yoga Items:', 'salient_child' ),
    'not_found'          => __( 'No Yoga Items found.', 'salient_child' ),
    'not_found_in_trash' => __( 'No Yoga Items found in Trash.', 'salient_child' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'menu_icon'          => 'dashicons-universal-access-alt',
    'menu_position'      => 9,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'yoga' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions' ),
  );

  register_post_type( 'yoga_workshops', $args );
}
add_action( 'init', 'salient_child_custom_post_type_yoga', 0 );
