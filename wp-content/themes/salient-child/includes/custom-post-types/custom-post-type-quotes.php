<?php
/**
 * Register quotes post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



function salient_child_custom_post_type_quotes() {
  
  $labels = array(
    'name'               => _x( 'Quotes', 'post type general name', 'salient_child' ),
    'singular_name'      => _x( 'Quote Item', 'post type singular name', 'salient_child' ),
    'menu_name'          => _x( 'Quote', 'admin menu', 'salient_child' ),
    'name_admin_bar'     => _x( 'Quote Item', 'add new on admin bar', 'salient_child' ),
    'add_new'            => _x( 'Add New', 'Quote Items', 'salient_child' ),
    'add_new_item'       => __( 'Add New Quote Item', 'salient_child' ),
    'new_item'           => __( 'New Quote Item', 'salient_child' ),
    'edit_item'          => __( 'Edit Quote Item', 'salient_child' ),
    'view_item'          => __( 'View Quote Item', 'salient_child' ),
    'all_items'          => __( 'All Quote Items', 'salient_child' ),
    'search_items'       => __( 'Search Quote Items', 'salient_child' ),
    'parent_item_colon'  => __( 'Parent Quote Items:', 'salient_child' ),
    'not_found'          => __( 'No Quote Items found.', 'salient_child' ),
    'not_found_in_trash' => __( 'No Quote Items found in Trash.', 'salient_child' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'menu_icon'          => 'dashicons-format-quote',
    'menu_position'      => 9,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'quotes' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions' ),
  );

  register_post_type( 'quotes', $args );
}
add_action( 'init', 'salient_child_custom_post_type_quotes', 0 );
