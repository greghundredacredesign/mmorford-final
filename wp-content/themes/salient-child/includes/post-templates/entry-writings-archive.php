<?php 
$options = get_nectar_theme_options(); 
global $post;

$masonry_size_pm = get_post_meta($post->ID, '_post_item_masonry_sizing', true); 
$masonry_item_sizing = (!empty($masonry_size_pm)) ? $masonry_size_pm : 'regular';
$using_masonry = null;
$masonry_type = (!empty($options['blog_masonry_type'])) ? $options['blog_masonry_type'] : 'classic';
$blog_type = $options['blog_type']; 
$blog_standard_type = (!empty($options['blog_standard_type'])) ? $options['blog_standard_type'] : 'classic';

global $layout;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($masonry_item_sizing); ?>>

	<?php do_action( 'salient_child_before_post' ); ?>

	<div class="inner-wrap animated">

	<div class="post-content post-content--writings post-content--writings-archive">

			<?php if ( has_post_thumbnail() ) : ?>
				<div class="post-featured-image">
					<a href="<?php echo get_permalink() ?>" class="img-link"><span class="post-featured-img"><?php echo get_the_post_thumbnail( $post->ID, 'medium' ); ?></span></a>
				</div>
			<?php endif; ?>

			<?php
				$extra_class = '';
				if (!has_post_thumbnail()) $extra_class = 'no-img';
				?>
			<div class="post-header <?php echo $extra_class; ?>">

				<div class="post-meta">
					<?php
					$use_excerpt = (!empty($options['blog_auto_excerpt']) && $options['blog_auto_excerpt'] == '1') ? 'true' : 'false'; 
					?>
					
					<span class="date"><?php echo get_the_date(); ?></span><!--/date-->
					<?php echo SalientChildRecentPostsWritingsShortcode()->get_post_publisher_meta( $post ); ?>
				</div><!--/post-meta-->

				<h2 class="title">
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</h2>
			</div><!--/post-header-->
			
		</div><!--/post-content-->

	</div><!--/inner-wrap-->
		
</article><!--/article-->