<?php 
$options = get_nectar_theme_options(); 

global $post;
setup_postdata( $post );

$masonry_size_pm = get_post_meta($post->ID, '_post_item_masonry_sizing', true); 
$masonry_item_sizing = (!empty($masonry_size_pm)) ? $masonry_size_pm : 'regular';
$using_masonry = null;
$masonry_type = (!empty($options['blog_masonry_type'])) ? $options['blog_masonry_type'] : 'classic';
$blog_type = $options['blog_type']; 
$blog_standard_type = (!empty($options['blog_standard_type'])) ? $options['blog_standard_type'] : 'classic';

global $layout;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($masonry_item_sizing); ?>>

	<?php do_action( 'salient_child_before_post' ); ?>

	<?php if ( has_post_thumbnail() ) {

	 global $options;
	 $hide_featrued_image = (!empty($options['blog_hide_featured_image'])) ? $options['blog_hide_featured_image'] : '0'; 
	 if( $hide_featrued_image != '1'){
	 	echo '<span class="post-featured-img">'.get_the_post_thumbnail($post->ID, 'full', array('title' => '')) .'</span>';
	 }	

	} ?>

	<div class="row heading-title" data-header-style="<?php echo $blog_header_type; ?>">
		<div class="col span_12 section-title blog-title">
			<?php if($blog_header_type == 'default_minimal') { ?> 
			<span class="meta-category">

					<?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {
								$output = null;
							    foreach( $categories as $category ) {
							        $output .= '<a class="'.$category->slug.'" href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', NECTAR_THEME_NAME), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>';
							    }
							    echo trim( $output);
							} ?>
					</span> 

			</span> <?php } ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div id="single-below-header">
				<span class="meta-date date updated"><?php echo get_the_date(); ?></span><?php echo SalientChildRecentPostsWritingsShortcode()->get_post_publisher_meta( $post->ID ); ?>
			</div><!--/single-below-header-->

			<?php if($blog_header_type != 'default_minimal') { ?>

				<?php } ?>
			</div>
		</div>

	<div class="inner-wrap animated">

		<div class="post-content">

			<?php
			$meta_overlaid_style = ($using_masonry == true && $masonry_type == 'meta_overlaid') ? true : false; ?>
			
			<?php 
				$img_size = ($blog_type == 'masonry-blog-sidebar' && substr( $layout, 0, 3 ) != 'std' || $blog_type == 'masonry-blog-fullwidth' && substr( $layout, 0, 3 ) != 'std' || $blog_type == 'masonry-blog-full-screen-width' && substr( $layout, 0, 3 ) != 'std' || $layout == 'masonry-blog-sidebar' || $layout == 'masonry-blog-fullwidth' || $layout == 'masonry-blog-full-screen-width') ? 'large' : 'full';
			 	if($using_masonry == true && $masonry_type == 'meta_overlaid') {
			 		$img_size = (!empty($masonry_item_sizing)) ? $masonry_item_sizing : 'portfolio-thumb';
			 	}
			 	if($using_masonry == true && $masonry_type == 'classic_enhanced') { 
			 		$img_size = (!empty($masonry_item_sizing) && $masonry_item_sizing == 'regular') ? 'portfolio-thumb' : 'full';


			 		if($img_size == 'regular') {
			 			$image_attrs =  array('title' => '', 'sizes' => '(min-width: 1600px) 20vw, (min-width: 1300px) 25vw, (min-width: 1000px) 33vw, (min-width: 690px) 100vw, 100vw');
			 		} else if($img_size == 'wide_tall') {
			 			$image_attrs =  array('title' => '', 'sizes' => '(min-width: 1600px) 25vw, (min-width: 1300px) 40vw, (min-width: 1000px) 50vw, (min-width: 690px) 50vw, 100vw');
			 		} else if($img_size == 'large_featured') {
			 			$image_attrs =  array('title' => '', 'sizes' => '(min-width: 690px) 100vw, 100vw');
			 		} else {
			 			$image_attrs =  array('title' => '', 'sizes' => '(min-width: 1600px) 20vw, (min-width: 1300px) 25vw, (min-width: 1000px) 33.3vw, (min-width: 690px) 50vw, 100vw');
			 		}
				 		
				 	 
			 	}

				if($using_masonry == true && $masonry_type == 'classic_enhanced') echo'<a href="' . get_permalink() . '" class="img-link"><span class="post-featured-img">'.get_the_post_thumbnail($post->ID, $img_size, $image_attrs) .'</span></a>'; 
			?>


			<?php 
			//minimal std
			if($using_masonry != true && $blog_standard_type == 'minimal') { ?>

				<div class="content-inner">

						<?php 
							//on the single post page display the content
							the_content('<span class="continue-reading">'. __("Read More", NECTAR_THEME_NAME) . '</span>'); 
					 	?>
						
						<?php global $options;
							if( $options['display_tags'] == true ){
								 
								if( has_tag() ) {
								
									echo '<div class="post-tags"><h4>'.__('Tags:').'</h4>'; 
									the_tags('','','');
									echo '<div class="clear"></div></div> ';
									
								}
							}
						?>

				</div><!--/content-inner-->


			<?php }

			//other styles
			else { ?>

				<div class="content-inner">
					
					<?php 

					if( !has_post_thumbnail() && $meta_overlaid_style == true) {

						//no image added
						$img_size = (!empty($masonry_item_sizing)) ? $masonry_item_sizing : 'portfolio-thumb';
						switch($img_size) {
							case 'large_featured':
								$no_image_size = 'no-blog-item-large-featured.jpg';
								break;
							case 'wide_tall':
								$no_image_size = 'no-portfolio-item-tiny.jpg';
								break;
							default:
								$no_image_size = 'no-portfolio-item-tiny.jpg';
								break;
						}
						 echo '<a href="' . get_permalink() . '"><span class="post-featured-img"><img src="'.get_template_directory_uri().'/img/'.$no_image_size.'" alt="no image added yet." /></span></a>';
				
					} ?>
				
					<?php 
						//on the single post page display the content
						the_content('<span class="continue-reading">'. __("Read More", NECTAR_THEME_NAME) . '</span>'); 
					?>
					
					<?php global $options;
						if( $options['display_tags'] == true ){
							 
							if( has_tag() ) {
							
								echo '<div class="post-tags"><h4>'.__('Tags:').'</h4>'; 
								the_tags('','','');
								echo '<div class="clear"></div></div> ';
								
							}
						}
					?>
						
				</div><!--/content-inner-->

			<?php } // other styles ?>
			
		</div><!--/post-content-->

	</div><!--/inner-wrap-->
		
</article><!--/article-->

<?php
	 wp_link_pages(); 
		

	    global $options; 

	  if($theme_skin != 'ascend') {
			if( !empty($options['author_bio']) && $options['author_bio'] == true){ 
				$grav_size = 80;
				$fw_class = null; 
			?>
				
				<div id="author-bio" class="<?php echo $fw_class; ?>">
					<div class="span_12">
						<?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), $grav_size, null, get_the_author() ); }?>
						<div id="author-info">
							<h3><span><?php if(!empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') { _e('Author', NECTAR_THEME_NAME); } else { _e('About', NECTAR_THEME_NAME); } ?></span> <?php the_author(); ?></h3>
							<p><?php the_author_meta('description'); ?></p>
						</div>
						<?php if(!empty($options['theme-skin']) && $options['theme-skin'] == 'ascend'){ echo '<a href="'. get_author_posts_url(get_the_author_meta( 'ID' )).'" data-hover-text-color-override="#fff" data-hover-color-override="false" data-color-override="#000000" class="nectar-button see-through-2 large"> '. __("More posts by",NECTAR_THEME_NAME) . ' ' .get_the_author().' </a>'; } ?>
						<div class="clear"></div>
					</div>
				</div>
				
		<?php } ?>

		<div class="comments-section">
			   <?php comments_template(); ?>
		 </div>   


	<?php } ?>

	<?php if($blog_header_type == 'default_minimal' && $blog_social_style != 'fixed_bottom_right')  { ?>
	
		<div class="bottom-meta">	
			<?php
				echo '<div class="sharing-default-minimal">'; 
					nectar_blog_social_sharing();
				echo '</div>'; ?>
		</div>
	<?php }


wp_reset_postdata();
