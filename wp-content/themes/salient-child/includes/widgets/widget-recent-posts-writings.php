<?php
/**
 * Implements Recent Posts Writings widget.
 */

function salient_child_recent_posts_writings_widget_init() {
	register_widget( 'SalientChildRecentPostsWritingsWidget' );
}
add_action('widgets_init', 'salient_child_recent_posts_writings_widget_init');



class SalientChildRecentPostsWritingsWidget extends WP_Widget {

  public function __construct() {
    $widget_ops = array(
			'classname' => 'SalientChildRecentPostsWritingsWidget',
			'description' => __( 'The most recent writing posts on your site.', 'salient_child' )
		);
		parent::__construct( 'recent_posts_writings', __( 'Recent Writings', 'salient_child'), $widget_ops);
		$this->alt_option_name = 'recent_posts_writings_widget';
  }



  public function widget( $args, $instance ) {

    if ( ! empty( $instance['title_tag'] ) ) {
      $args['before_title'] = '<' . esc_attr( $instance['title_tag'] ) . ' class="widget-title cbb-about__title">';
      $args['after_title'] = '</' . esc_attr( $instance['title_tag'] ) . '>';
    }

    $style = ! empty( $instance['style'] ) ? $instance['style'] : '';
    $columns = ! empty( $instance['columns'] ) ? $instance['columns'] : '';
    $posts_per_page = ! empty( $instance['posts_per_page'] ) ? $instance['posts_per_page'] : '';
    $posts_offset = ! empty( $instance['posts_offset'] ) ? $instance['posts_offset'] : '';
    
    $shortcode_atts_string = '';
    if ( !empty( $style ) ) { $shortcode_atts_string .= ' style="' . $style . '"'; }
    if ( absint( $columns ) ) { $shortcode_atts_string .= ' columns="' . $columns . '"'; }
    if ( absint( $posts_per_page ) ) { $shortcode_atts_string .= ' posts_per_page="' . $posts_per_page . '"'; }
    if ( absint( $posts_offset ) ) { $shortcode_atts_string .= ' posts_offset="' . $posts_offset . '"'; }

    echo $args['before_widget'];
    echo do_shortcode( '[recent_posts_writings ' . $shortcode_atts_string . ']' );
    echo $args['after_widget'];
  }



  public function form( $instance ) {
    $style = ! empty( $instance['style'] ) ? $instance['style'] : '';
    $columns = ! empty( $instance['columns'] ) ? $instance['columns'] : '';
    $posts_per_page = ! empty( $instance['posts_per_page'] ) ? $instance['posts_per_page'] : '';
    $posts_offset = ! empty( $instance['posts_offset'] ) ? $instance['posts_offset'] : '';

    ?>
    <p>
    <label><?php _e( 'Style:', 'salient_child' ); ?></label>
    <br>
    <select class="widefat" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
      <option value="minimal-custom" <?php echo esc_attr( $style == 'minimal-custom' ? 'selected' : '' ); ?>><?php _e( 'Minimal Custom', 'salient_child' ); ?></option>
      <option value="default" <?php echo esc_attr( $style == 'default' ? 'selected' : '' ); ?>><?php _e( 'Default', 'salient_child' ); ?></option>
      <option value="minimal" <?php echo esc_attr( $style == 'minimal' ? 'selected' : '' ); ?>><?php _e( 'Minimal', 'salient_child' ); ?></option>
      <option value="title_only" <?php echo esc_attr( $style == 'title_only' ? 'selected' : '' ); ?>><?php _e( 'Minimal - Title Only', 'salient_child' ); ?></option>
    </select>
    <small><?php _e( 'Please select desired style here.', 'salient_child' ) ?></small>
  	</p>

  	<p>
    <label><?php _e( 'Number of Columns:', 'salient_child' ); ?></label>
    <br>
    <select class="widefat" id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>">
      <option value="1" <?php echo esc_attr( $columns == '1' ? 'selected' : '' ); ?>><?php _e( '1', 'salient_child' ); ?></option>
      <option value="2" <?php echo esc_attr( $columns == '2' ? 'selected' : '' ); ?>><?php _e( '2', 'salient_child' ); ?></option>
      <option value="3" <?php echo esc_attr( $columns == '3' ? 'selected' : '' ); ?>><?php _e( '3', 'salient_child' ); ?></option>
      <option value="4" <?php echo esc_attr( $columns == '4' ? 'selected' : '' ); ?>><?php _e( '4', 'salient_child' ); ?></option>
    </select>
    <small><?php _e( 'Please select the number of posts you would like to display.', 'salient_child' ) ?></small>
  	</p>

    <p>
    <label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>"><?php _e( 'Number of Posts:', 'salient_child' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" type="text" value="<?php echo esc_attr( $posts_per_page ); ?>">
    <small><?php _e( 'How many posts would you like to display? <br>Enter as a number example "4"', 'salient_child' ) ?></small>
    </p>

    <p>
    <label for="<?php echo $this->get_field_id( 'posts_offset' ); ?>"><?php _e( 'Posts Offset:', 'salient_child' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'posts_offset' ); ?>" name="<?php echo $this->get_field_name( 'posts_offset' ); ?>" type="text" value="<?php echo esc_attr( $posts_offset ); ?>">
    <small><?php _e( 'Optioinally enter a number e.g. "2" to offset your posts by - useful for when you\'re using multiple styles of this element on the same page and would like them to no show duplicate posts.', 'salient_child' ) ?></small>
    </p>

    <?php 
  }



  public function update( $new_instance, $old_instance ) {
    $instance = array();

    $style = ! empty( $instance['style'] ) ? $instance['style'] : '';
    $columns = ! empty( $instance['columns'] ) ? $instance['columns'] : '';
    $posts_per_page = ! empty( $instance['posts_per_page'] ) ? $instance['posts_per_page'] : '';
    $posts_offset = ! empty( $instance['posts_offset'] ) ? $instance['posts_offset'] : '';

    $instance['style'] = ( ! empty( $new_instance['style'] ) ) ? sanitize_text_field( $new_instance['style'] ) : '';
    $instance['columns'] = absint( $new_instance['columns'] ) ? absint( $new_instance['columns'] ) : '';
    $instance['posts_per_page'] = absint( $new_instance['posts_per_page'] ) ? absint( $new_instance['posts_per_page'] ) : '';
    $instance['posts_offset'] = absint( $new_instance['posts_offset'] ) ? absint( $new_instance['posts_offset'] ) : '';

    return $instance;
  }
}