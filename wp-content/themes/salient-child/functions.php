<?php 

// Load custom post types
require get_stylesheet_directory() . '/includes/custom-post-types/custom-post-type-yoga.php';
require get_stylesheet_directory() . '/includes/custom-post-types/custom-post-type-writings.php';
require get_stylesheet_directory() . '/includes/custom-post-types/custom-post-type-quotes.php';

// Load custom taxonomies
require get_stylesheet_directory() . '/includes/taxonomies/taxonomy-class-types.php';
require get_stylesheet_directory() . '/includes/taxonomies/taxonomy-publishers.php';

// Load widget areas
require get_stylesheet_directory() . '/includes/widget-areas.php';

// Load shortcodes
require get_stylesheet_directory() . '/includes/shortcodes/shortcode-recent-posts-writings.php';
require get_stylesheet_directory() . '/includes/shortcodes/shortcode-recent-posts-yoga.php';

// Load widgets
require get_stylesheet_directory() . '/includes/widgets/widget-recent-posts-writings.php';

// Load custom features
require get_stylesheet_directory() . '/includes/infinite-scroll.php';
require get_stylesheet_directory() . '/includes/custom-logo.php';
require get_stylesheet_directory() . '/includes/facetwp-filters.php';

// Load custom visual composer features
require get_stylesheet_directory() . '/includes/vc-features/vc-recent-writings.php';
require get_stylesheet_directory() . '/includes/vc-features/vc-recent-yoga.php';



/**
 * Enqueue child theme styles and scripts.
 */
function salient_child_enqueue_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

  if ( is_rtl() ) {
		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
  }
}
add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');


