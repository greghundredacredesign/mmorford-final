/**
 * File infinite-scroll.js.
 *
 * Load next post via ajax and append to the page.
 *
 * Author: Diego Versiani
 * Contact: https://diegoversiani.me/
 */

(function( $ ){

  'use strict';

  // Run initialize on pageload
  window.addEventListener( 'load', init );

  

  var marker,
      marker_selector = '.infinite-scroll-marker',
      post_container_selector = '#post-area',
      next_post_field_selector = 'input.next-post-id',
      end_of_posts_reached = false
      ;


  /**
   * Initialize component and set related handlers
   */
  function init( e ) {
    marker = document.querySelector( marker_selector );

    // Load next post when get to bottom of the last loaded post
    inView( marker_selector ).on( 'enter', loadNextPost );

    // Add loading event listeners
    $(document).on( 'loading_next_post', displaySpinner );
    $(document).on( 'loaded_next_post', hideSpinner );
  };



  //
  // METHODS
  //

  function displaySpinner() {
    marker.innerHTML = '<div class="loading-icon none" style=""><div class="material-icon"><div class="spinner"><div class="right-side"><div class="bar"></div></div><div class="left-side"><div class="bar"></div></div></div><div class="spinner color-2"><div class="right-side"><div class="bar"></div></div><div class="left-side"><div class="bar"></div></div></div></div> </div>';
  };

  function hideSpinner() {
    marker.innerHTML = '';
  };



  function loadNextPost() {
    if ( !end_of_posts_reached ) {
      var loaded_posts = document.querySelectorAll( '#post-area > article .next-post-id' );
      var post_id = loaded_posts[loaded_posts.length - 1].value;
      requestNextPostContent( post_id );
    }
  };



  /**
   * Get next post content from server.
   */
  function requestNextPostContent( post_id ) {
    var post_data = {
      action: 'salient_child_get_next_post',
      post_id: post_id,
    };

    // Trigger loading_next_post event
    $( document.body ).trigger( 'loading_next_post', [ post_data ] );

    // Sent post
    $.post( AjaxParams.ajax_url, post_data, processRequestNextPostContent );
  };



  /**
   * Handle a successful request for adding product to cart
   */
  function processRequestNextPostContent( response ) {
    // Trigger loading_next_post event
    $( document.body ).trigger( 'loaded_next_post', [ response.post_id ] );
      
    // Display messages
    if ( response.message ) {
      var post_container = document.querySelector( post_container_selector );
      post_container.innerHTML += '<p>' + response.message + '</p>';
    }

    // Append post content
    if ( response.post_content ) {
      var post_container = document.querySelector( post_container_selector );
      post_container.innerHTML += response.post_content;
    }

    // Prevent displaying end_of_posts message again
    if ( response.end_of_posts ) {
      end_of_posts_reached = true;
    }

    // Load next post if marker is visible
    if ( inView.is( marker ) ) {
      loadNextPost();
    }
  };

})( jQuery );
