/**
 * File facetwp-init.js.
 *
 * Initialize facetwp and handle it's events.
 *
 * Author: Diego Versiani
 * Contact: https://diegoversiani.me/
 */

(function( $ ){

  'use strict';

  // Run initialize on pageload
  window.addEventListener( 'load', init );

  

  /**
   * Initialize component and set related handlers
   */
  function init( e ) {
    // Add FacetWP refresh/loaded handler
    if ( jQuery !== undefined && window.FWP !== undefined ) {
      jQuery(document).on( 'facetwp-loaded', handleFacetWPLoaded );
    }
  };



  //
  // METHODS
  //

  /**
   * Handle FacetWP refresh/loaded event
   */
  function handleFacetWPLoaded( e ) {
    var animatedElements = document.querySelectorAll( '.facetwp-template .animated' );

    for (var i = animatedElements.length - 1; i >= 0; i--) {
      animatedElements[i].classList.remove( 'animated' );
    }
  };

})( jQuery );
