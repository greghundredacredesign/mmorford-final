<?php
/*
* Shortcode weekly-classes
* Template: Simple
*/
?>

<div class="weekly-classes">
  <table class="weekly-classes__table">
    <?php
      $previous_row = null;
      foreach ( $calendar_data as $row ) : ?>
        
        <tr>
          <td class="weekly-classes__day">
            <?php if ( !$previous_row || $previous_row[0] != $row[0] ) { echo '<span>' . $row[0] . '</span>'; } ?>
          </td>
          <td class="weekly-classes__value">
            <?php
            for ( $i = 1;  $i < count( $row ); $i++ ) {
              $separator = $i > 1 ? ' | ' : '';
              echo '<span class="weekly-classes__entry-column' . $i . '">' . $separator. $row[ $i ] . '</span>';
            }
            ?>
          </td>
        </tr>

        <?php
        $previous_row = $row;
      endforeach;
      ?>

  </table>
</div>
