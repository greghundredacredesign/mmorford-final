<?php
/*
Plugin Name: Weekly Classes for Mark Morford
Plugin URI:
Description: Provide shortcodes and widgets to display weekly classes calendar.
Version: 1.0.0
Author: Diego Versiani
Author URI: https://diegoversiani.me/
*/

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}




define( 'WEEKLY_CLASSES_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'WEEKLY_CLASSES_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'WEEKLY_CLASSES_THEME_TEMPLATES_FOLDER', 'plugins/weekly-classes/templates/' );
define( 'WEEKLY_CLASSES_TEMPLATES_FOLDER',  WEEKLY_CLASSES_PLUGIN_PATH . 'templates/' );


/*
* Load main plugin class
*/
require_once( WEEKLY_CLASSES_PLUGIN_PATH . 'inc/classes/class.weekly-classes.php' );

/*
* Load custom post types
*/
require_once( WEEKLY_CLASSES_PLUGIN_PATH . 'inc/custom-post-types/custom-post-type-weekly-classes.php' );

/*
* Load shortcodes
*/
require_once( WEEKLY_CLASSES_PLUGIN_PATH . 'inc/shortcodes/shortcode-weekly-classes.php' );
