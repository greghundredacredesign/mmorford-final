<?php
/**
 * Register weekly-classes post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



function weekly_classes_custom_post_type_weekly_classes() {
  
  $labels = array(
    'name'               => _x( 'Weekly Classes', 'post type general name', 'weekly_classes' ),
    'singular_name'      => _x( 'Weekly Classes', 'post type singular name', 'weekly_classes' ),
    'menu_name'          => _x( 'Weekly Classes', 'admin menu', 'weekly_classes' ),
    'name_admin_bar'     => _x( 'Weekly Classes', 'add new on admin bar', 'weekly_classes' ),
    'add_new'            => _x( 'Add New', 'Weekly Classes', 'weekly_classes' ),
    'add_new_item'       => __( 'Add New Weekly Classes', 'weekly_classes' ),
    'new_item'           => __( 'New Weekly Classes', 'weekly_classes' ),
    'edit_item'          => __( 'Edit Weekly Classes', 'weekly_classes' ),
    'view_item'          => __( 'View Weekly Classes', 'weekly_classes' ),
    'all_items'          => __( 'All Weekly Classes', 'weekly_classes' ),
    'search_items'       => __( 'Search Weekly Classes', 'weekly_classes' ),
    'parent_item_colon'  => __( 'Parent Weekly Classes:', 'weekly_classes' ),
    'not_found'          => __( 'No Weekly Classes found.', 'weekly_classes' ),
    'not_found_in_trash' => __( 'No Weekly Classes found in Trash.', 'weekly_classes' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => false,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'weekly-classes' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'supports'           => array( 'title' )
  );

  register_post_type( 'weekly_classes', $args );
}
add_action( 'init', 'weekly_classes_custom_post_type_weekly_classes', 0 );



function weekly_classes_custom_post_type_metaboxes() {
  $screen = get_current_screen();

  // USAGE METABOX
  // Displayed only when editing `weekly_classes`
  if ( $screen->post_type == 'weekly_classes'
        && $screen->base == 'post'
        && empty( $screen->action ) ) {
    add_meta_box(
      'weekly_classes_products_carousel_usage',
      __( 'Usage', 'weekly_classes' ),
      'weekly_classes_custom_post_type_usage_metabox_render',
      'weekly_classes',
      'normal',
      'high'
    );
  }

  // MAIN METABOX
  add_meta_box(
    'weekly_classes_custom_post_type_main',
    __( 'Classes', 'weekly_classes' ),
    'weekly_classes_custom_post_type_main_metabox_render',
    'weekly_classes',
    'normal',
    'high'
  );
}
add_action( 'add_meta_boxes_weekly_classes', 'weekly_classes_custom_post_type_metaboxes' );



function weekly_classes_custom_post_type_usage_metabox_render() {
  global $post;
  ?>
    <p>
      <?php esc_html_e( 'Copy the shortcode below and paste in a post, page or text widget:', 'weekly_classes' ) ?>
    </p>
    <p>
      <input type="text" value="<?php echo esc_html( '[weekly-classes id="' . esc_attr( $post->ID ) . '"]' ); ?>" class="widefat" readonly>
    </p>
  <?php
}



function weekly_classes_custom_post_type_main_metabox_render() {
  global $post;

  $post_meta = WeeklyClasses()->get_post_meta( $post->ID, 'weekly_classes' );

  $classes_input = isset( $post_meta['classes_input'] ) ? $post_meta['classes_input'] : '';

  // create nonce
  wp_nonce_field( basename( __FILE__ ), 'classes_input_carousel_meta_nonce' );
  ?>
    <p>
      <label for="post_meta[classes_input]"><?php _e( 'Classes:', 'weekly_classes' ); ?></label><br>
      <textarea name="post_meta[classes_input]" id="post_meta[classes_input]" rows="10" class="widefat"><?php echo esc_attr( $classes_input ); ?></textarea>
      <span class="description">Please enter the classes calendar in the following format:</span><br>
<pre>
MON|6:15pm|All Levels
TUE|9:00am|All Levels
WED|6:15pm|Level 2-3
THU|9:00am|All Levels
SUN|9:00am|Level 2-3|Absolution Flow
SUN|11:00am|All Levels|Absolution Flow
</pre>
    </p>
  <?php
}



/**
 * Save metaboxes content.
 */
function weekly_classes_weekly_classes_metaboxes_save( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  // Check the user's permissions
  if ( !current_user_can( 'edit_post', $post_id ) ) {
    return;
  }

  // CONTENT BLOCK FIELDS
  if ( isset( $_POST['classes_input_carousel_meta_nonce'] )
        && wp_verify_nonce( $_POST['classes_input_carousel_meta_nonce'], basename( __FILE__ ) ) ) {
    
    $post_meta = WeeklyClasses()->get_post_meta( $post_id, 'weekly_classes' );

    $post_meta['classes_input'] = sanitize_textarea_field( $_POST['post_meta']['classes_input'] );

    // Update post meta
    update_post_meta( $post_id, 'weekly_classes_meta', $post_meta );
  }

}
add_action( 'save_post_weekly_classes', 'weekly_classes_weekly_classes_metaboxes_save' );



/**
 * Modify column for weekly classes post type.
 * @param  array $columns   Post type columns.
 * @return array            Modified post type columns
 */
function weekly_classes_custom_post_type_column_shortcode( $columns ) {
  $columns['shortcode'] = 'Shortcode'; // add Shortcode
  unset( $columns['date'] ); // Remove date
  return $columns;
}
add_filter( 'manage_weekly_classes_posts_columns', 'weekly_classes_custom_post_type_column_shortcode', 10 );



/**
 * Display content for shortcode column of products carousel post type.
 * @param  string $column_name Column name.
 * @param  int    $post_ID     Post ID.
 */
function weekly_classes_custom_post_type_column_shortcode_content( $column_name, $post_ID ) {
  if ( $column_name == 'shortcode' ) : ?>
    <input type="text" value="<?php echo esc_html( '[weekly-classes id="' . esc_attr( $post_ID ) . '"]' ); ?>" class="widefat" readonly>
  <?php endif;
}
add_action( 'manage_weekly_classes_posts_custom_column', 'weekly_classes_custom_post_type_column_shortcode_content', 10, 2 );

