<?php
/**
 * Implements weekly-classes shortcode.
 *
 * @package weekly_classes
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



class WeeklyClassesShortcode {
  
  protected static $_instance = null;

  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }

    return self::$_instance;
  }



  public function __construct() {
    add_action( 'init', array( $this, 'register_shortcode' ), 0 );

    // Register templates
    WeeklyClasses()->register_template( array(
        'group'              => 'weekly-classes',
        'name'               => 'simple',
        'label'              => __( 'Simple', 'weekly_classes' ),
      ) );
  }





  public function register_shortcode() {
    add_shortcode('weekly-classes', array( $this, 'render_shortcode' ) );
  }





  public function render_shortcode( $atts ) {
    extract(shortcode_atts(array(
      "id" => null,
      ), $atts)
    );

    $return_string = '';

    // Bail early if none id was passed
    if( empty( $id ) ) {
      return '';
    }

    // Get post
    global $post;
    $post = get_post( $id );

    // Bail if wrong post type
    if ( $post->post_type != 'weekly_classes' ) {
      wp_reset_postdata();
      return '';
    }

    setup_postdata( $post );

    // Get meta
    $post_meta = WeeklyClasses()->get_post_meta( $post->ID, 'weekly_classes' );

    // Mount calendar array
    $calendar_data = array();
    $input_lines = explode( PHP_EOL, $post_meta['classes_input'] );
    foreach ( $input_lines as $line ) {
      if ( empty( $line ) ) { continue; }
      $calendar_data[] = explode( '|', $line );
    }

    // Get template
    $templates = WeeklyClasses()->get_templates();
    $template = $templates[ 'weekly-classes-simple' ];

    // Start buffering
    ob_start();

    $template_file = WeeklyClasses()->locate_template( $template['group'], $template['name'] );
    include ( $template_file );

    // Get buffer contents and release
    $return_string .= ob_get_clean();

    // Reset post data
    wp_reset_postdata();

    // Enqueue scripts and styles
    wp_enqueue_style( 'weekly-classes' );

    return $return_string;
  }
}



function WeeklyClassesShortcode() {
  return WeeklyClassesShortcode::instance();
}



// Get instance to initialize class instance
WeeklyClassesShortcode();
