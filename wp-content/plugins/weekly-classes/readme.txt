=== Weekly Classes for Mark Morford ===
Contributors: diegoversiani
Donate link:
Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.0.0

Provide shortcodes and widgets to display weekly classes calendar.


== Description ==

Provide shortcodes and widgets to display weekly classes calendar.


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload \`mmorford-weekly-classes\` to the \`/wp-content/plugins/\` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0 =

